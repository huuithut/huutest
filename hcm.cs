﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using INet.Mercury;
using INet.Mercury.Model;
using INet;
using INet.EdXml.Util;
using System.Reflection;
using System.IO;
using INet.EdXml;
using INet.EdXml.Header;
using INet.EdXml.Attachment;
using System.Threading;
using System.Diagnostics;
using INet.Morpheus;
using System.Web.Services;
using System.Collections;
using System.Xml;
using INet.StatusXml;

namespace DemoBTT
{
    public partial class hcm : Form
    {
        DataTable dtDsDonvi;
        TDGetEmailList.Service tdservice = new TDGetEmailList.Service();
        TDSecurity tdKey = new TDSecurity();
        System.Timers.Timer timer;
        LotusConnection lotus;
        GetAgencyResponse thongtindonvi;
        MercuryServiceConfig hcmconfig;
        MercuryService hcmservice;
        LogVanBan logs;
        bool IsConnectedToServer = false;
        bool isC = false;
        string checktimerwork = "none";
        public hcm()
        {
            InitializeComponent();
        }
        private void hcm_Load(object sender, EventArgs e)
        {
            GetInfo();
        }
        public string[] getFiles(string SourceFolder, string Filter, System.IO.SearchOption searchOption)
        {
            ArrayList alFiles = new ArrayList();
            string[] MultipleFilters = Filter.Split('|');
            foreach (string FileFilter in MultipleFilters)
            {
                alFiles.AddRange(Directory.GetFiles(SourceFolder, FileFilter, searchOption));
            }
            return (string[])alFiles.ToArray(typeof(string));
        }
        private void GetInfo()
        {
            txtApiUrl.Text = CommonClass.HcmServerUrl;
            txtTaiKhoan.Text = tdKey.TDDecrypt(CommonClass.AccessKey);
            txtMatKhau.Text = tdKey.TDDecrypt(CommonClass.SecretKey);
            txtTDUrl.Text = CommonClass.TDServerUrl;
            txtTDUser.Text = CommonClass.TDUsername;
            txtTDPw.Text = tdKey.TDDecrypt(CommonClass.TDPassword);
            txtFtpUrl.Text = CommonClass.FtpServer;
            txtFtpUsername.Text = CommonClass.FtpUserAccount;
            txtFtpPassword.Text = tdKey.TDDecrypt(CommonClass.FtpUserPassword);
            txtVBGui.Text = CommonClass.TDVbdi;
            txtVBNhan.Text = CommonClass.TDVbden;
            txtFolder.Text = CommonClass.TDFolder;
            txtApplicationName.Text = CommonClass.TDApplicationName;
            string serviceurl = CommonClass.GetServiceLocation().Replace(txtTDUrl.Text + "/", "").ToLower();
            if (serviceurl.Contains("http"))
            {
                serviceurl = string.Empty;
            }
            txtServices.Text = serviceurl;  //CommonClass.GetServiceLocation().Replace(txtTDUrl.Text + "/", "");
        }
        private void GetListOfOrgans()
        {
            try
            {
                hcmconfig = new MercuryServiceConfig();
                hcmconfig.ServiceUrl = txtApiUrl.Text;
                hcmconfig.ApplicationName = CommonClass.TDApplicationName;
                hcmservice = IWSClientFactory.CreateMercuryService(txtTaiKhoan.Text, txtMatKhau.Text, hcmconfig);
                CapNhatDanhMucDonVi.Service botttt = new CapNhatDanhMucDonVi.Service();
                //DataTable dt = new DataTable("Orgs");
                //dt.Columns.Add("Name", typeof(string));
                //dt.Columns.Add("Code", typeof(string));
                GetListAgenciesResponse listResponse = hcmservice.GetListAgencies();
                
                if (listResponse.Agencies != null)
                {
                    foreach (var agency in listResponse.Agencies)
                    {
                        string result = CommonClass.StringToStringBase64("Đơn vị liên thông" + ";" + agency.Name + ";" + agency.Name + ";" + agency.Code + ";tphcm;1");
                        //CapNhatDanhMucDonVi.EmailLog[] themmoi = botttt.LuuDanhSachBoTTTT("tudien.nsf", result, "");
                        //dt.Rows.Add(agency.Name, agency.Code);
                    }
                }
                else
                {
                    MessageBox.Show("null");
                }
                //dt.WriteXml("orgs.xml");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }
        private DataTable DanhSachNguoiNhan()
        {
            DataTable dt = new DataTable("Orgs");
            dt.Columns.Add("Name", typeof(string));
            dt.Columns.Add("Code", typeof(string));
            GetListAgenciesResponse listResponse = hcmservice.GetListAgencies();
            foreach (var agency in listResponse.Agencies)
            {
                dt.Rows.Add(agency.Name, agency.Code);
            }
            return dt;
        }
        private void RecieveDoc()
        {
            //thong tin cua don vi dang su dung phan mem lay van ban
            thongtindonvi = hcmservice.GetAgency();

            var checkKnobStickResponse = hcmservice.CheckKnobStick(new CheckKnobStickRequest().WithType(KnobStickType.ElectronicDocument));
            
            foreach (var knobstick in checkKnobStickResponse.KnobStickMetadatas)
            {
                var processKnobStick = new ProcessKnobStickRequest()
                        .WithId(knobstick.Id)
                        .WithStatus(ProcessStatus.processing);
                var processKnobstickResponse = hcmservice.ProcessKnobStick(processKnobStick);
                if (string.Equals("OK", processKnobstickResponse.Status, StringComparison.Ordinal))
                {
                    try
                    {
                        var getKnobstickResponse = hcmservice.GetKnobStick(new GetKnobStickRequest().WithId(knobstick.Id));
                        getKnobstickResponse.WriteResponseStreamToFile("temp\\" + knobstick.Id + ".edxml");
                        getKnobstickResponse.Dispose();
                        if (ExtractEdxml("temp\\" + knobstick.Id + ".edxml"))
                            hcmservice.ProcessKnobStick(processKnobStick.WithStatus(ProcessStatus.done));
                        else
                            hcmservice.ProcessKnobStick(processKnobStick.WithStatus(ProcessStatus.fail));
                    }
                    catch (Exception ex)
                    {
                        hcmservice.ProcessKnobStick(processKnobStick.WithStatus(ProcessStatus.fail));
                    }
                }
            }
        }
        private void SendDoc(string sendto, string documentid)
        {
            try
            {
                //hcmconfig = new MercuryServiceConfig();
                //hcmconfig.ServiceUrl = txtApiUrl.Text;
                //hcmconfig.ApplicationName = CommonClass.TDApplicationName;
                //hcmservice = IWSClientFactory.CreateMercuryService(txtTaiKhoan.Text, txtMatKhau.Text, hcmconfig);

                //thong tin cua don vi dang su dung phan mem lay van ban
                thongtindonvi = hcmservice.GetAgency();
                GetSlotResponse slotResponse = hcmservice.RequestSlot(new GetSlotRequest().WithType("edoc"));
                SendKnobStickResponse knobStickResponse = null;
                using (var stream = System.IO.File.Open("temp\\" + documentid + ".edxml", System.IO.FileMode.Open))//Assembly.GetExecutingAssembly().GetManifestResourceStream("Sender.example.edxml"))
                {
                    System.Threading.Thread.Sleep(5000);
                    SendKnobStickRequest knobstickRequest = new SendKnobStickRequest()
                            .WithSlot(slotResponse.Slot.Id)
                            .WithContent(stream)
                            .WithKey(documentid + ".edxml");
                    knobStickResponse = hcmservice.SendKnobStick(knobstickRequest);
                    stream.Dispose();
                }
                var deliverKnobstickRequest = new DeliverKnobStickRequest().WithId(knobStickResponse.KnobStickMetadata.Id);
                var deliverKnobstickResponse = hcmservice.DeliverKnobStick(deliverKnobstickRequest);
                if (string.Equals("OK", deliverKnobstickResponse.Status, StringComparison.Ordinal))
                {
                    string serverurl = CommonClass.TDVbdi;
                    if (!string.IsNullOrEmpty(CommonClass.TDFolder))
                    {
                        serverurl = CommonClass.TDFolder + "/" + serverurl;
                    }
                    XoaVBDaGui.Service tdxoavb = new XoaVBDaGui.Service();
                    XoaVBDaGui.GuiSMS[] thuchienxoa = tdxoavb.XoaMailVanBanDiCanGuiDi(documentid, serverurl);
                    tdxoavb.Dispose();
                }

                //hcmservice.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Loi gui van ban::::" + ex.Message + ex.StackTrace);
            }
        }
        private void SendDoc(string sendto, string documentid, string docgocid, TDOfficeService.MailSmsServicesService services)
        {
            try
            {         
                //thong tin cua don vi dang su dung phan mem lay van ban
                thongtindonvi = hcmservice.GetAgency();
                GetSlotResponse slotResponse = hcmservice.RequestSlot(new GetSlotRequest().WithType(KnobStickType.StatusDocument));
                SendKnobStickResponse knobStickResponse = null;
                using (var stream = System.IO.File.Open("temp\\Status-" + documentid + ".edxml", System.IO.FileMode.Open))
                {
                    System.Threading.Thread.Sleep(5000);
                    SendKnobStickRequest knobstickRequest = new SendKnobStickRequest()
                            .WithSlot(slotResponse.Slot.Id)
                            .WithContent(stream)
                            .WithKey("Status-" + documentid + ".edxml");
                    knobStickResponse = hcmservice.SendKnobStick(knobstickRequest);
                    stream.Dispose();
                }
                var deliverKnobstickRequest = new DeliverKnobStickRequest().WithId(knobStickResponse.KnobStickMetadata.Id);
                var deliverKnobstickResponse = hcmservice.DeliverKnobStick(deliverKnobstickRequest);
                if (string.Equals("OK", deliverKnobstickResponse.Status, StringComparison.Ordinal))
                {
                    string serverurl = CommonClass.TDVbdi;
                    if (!string.IsNullOrEmpty(CommonClass.TDFolder))
                    {
                        serverurl = CommonClass.TDFolder + "/" + serverurl;
                    }
                    services.CapNhatVBLT(docgocid, documentid, txtFolder.Text.Trim());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Loi gui Status::::" + ex.Message + ex.StackTrace);
            }
        }
        private void CapNhatTrangThai()
        {
            try
            {
                TDOfficeService.MailSmsServicesService ss = new TDOfficeService.MailSmsServicesService();
                var dsvblt = ss.getListOfReturnStatus(txtFolder.Text.Trim());
                if (dsvblt != null && dsvblt.Length > 0)
                {
                    foreach (string vblt in dsvblt)
                    {
                        if (!string.IsNullOrEmpty(vblt))
                        {
                            string[] thongtins = vblt.Split(';');
                            INet.StatusXml.StatusXml sendStatusXML = new INet.StatusXml.StatusXml();
                            INet.StatusXml.StatusXmlSoap sendStatusSoapXML = new INet.StatusXml.StatusXmlSoap();
                            INet.StatusXml.SOAP.SOAPHeader.Status sendStatus = new INet.StatusXml.SOAP.SOAPHeader.Status();
                            string sokyhieu = thongtins[4];
                            string lastchar = "";
                            if (!string.IsNullOrEmpty(sokyhieu))
                            {
                                lastchar = sokyhieu.Substring(sokyhieu.Length - 1);
                                if (lastchar.Equals("/"))
                                    sokyhieu = sokyhieu.Substring(0, sokyhieu.LastIndexOf("/"));
                            }
                            sendStatus.ResponseFor = new INet.StatusXml.SOAP.SOAPHeader.ResponseFor() { OrganId = thongtins[3], PromulgationDate = thongtins[5], Code = sokyhieu };
                            sendStatus.From = new INet.StatusXml.SOAP.SOAPHeader.From()
                            {
                                OrganId = thongtindonvi.AgencyResult.Code,
                                OrganName = thongtindonvi.AgencyResult.Name,
                                OrganizationInCharge = "",
                                OrganAdd = "",
                                Email = "",
                                Telephone = "",
                                Fax = "",
                                Website = ""
                            };
                            sendStatus.Description = thongtins[7];
                            sendStatus.StatusCode = thongtins[6];
                            sendStatus.Timestamp = thongtins[8];
                            sendStatus.StaffInfo = new INet.StatusXml.SOAP.SOAPHeader.StaffInfo() { Department = thongtins[9], Staff = thongtins[10] };
                            sendStatusSoapXML.Envelope.Header.Status = sendStatus;
                            sendStatusXML.StatusXmlSoap = sendStatusSoapXML;
                            sendStatusXML.ToFile("temp\\Status-" + thongtins[0] + ".edxml");

                            SendDoc("", thongtins[0], thongtins[1], ss);

                            //CommonClass.DeleteFileFromTemp("temp\\Status-" + thongtins[0] + ".edxml");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("VBLT::" + ex.Message + ex.StackTrace);
            }
        }
        private void DeleteSpamFile()
        {
            string[] sFiles = getFiles(Application.StartupPath, "*.rar|*.zip", SearchOption.AllDirectories);
            foreach (string FileName in sFiles)
            {
                CommonClass.DeleteFileFromTemp(FileName);
            }
        }
        private void BuildDocPackage()
        {
            try
            {
                string serverurl = CommonClass.TDVbdi;
                if (!string.IsNullOrEmpty(CommonClass.TDFolder))
                {
                    serverurl = CommonClass.TDFolder + "/" + serverurl;
                }
                TDGetEmailList.GuiSMS[] listofsending = tdservice.LayDanhSachEmailVBdiCanGuiDi(serverurl);
                if (listofsending != null)
                {
                    foreach (TDGetEmailList.GuiSMS sending in listofsending)
                    {
                        string xmlcontent = string.Empty;
                        xmlcontent = CommonClass.GetString(Convert.FromBase64String(sending.Content));
                        DataTable dt = CommonClass.StringXmlToDataSet(xmlcontent).Tables[0];
                        DataRow row = dt.Rows[0];
                        int countattachments = 0;
                        string dinhkemname = string.Empty;
                        if (sending != null)
                        {
                            //EdXml docPack = new EdXml();
                            INet.EdXml2.EdXml2 docPack = new INet.EdXml2.EdXml2();
                            //thong tin don vi gui den
                            INet.EdXml2.Header.From noiguiden = new INet.EdXml2.Header.From();
                            noiguiden.OrganId = thongtindonvi.AgencyResult.Code;
                            noiguiden.OrganName = thongtindonvi.AgencyResult.Name;
                            //nguoi ky
                            INet.EdXml2.Header.Author nguoiky = new INet.EdXml2.Header.Author();
                            nguoiky.FullName = CommonClass.GetString(Convert.FromBase64String(sending.STRNGUOIKY));
                            nguoiky.Function = row["CHUCVU"].ToString();
                            nguoiky.Competence = "Competence";

                            INet.EdXml2.Header.SignerInfo snguoiky = new INet.EdXml2.Header.SignerInfo();
                            snguoiky.FullName = CommonClass.GetString(Convert.FromBase64String(sending.STRNGUOIKY));
                            snguoiky.Position = row["CHUCVU"].ToString();
                            snguoiky.Competence = "Competence";

                            //so ky hieu
                            INet.EdXml2.Header.Code sokyhieu = new INet.EdXml2.Header.Code();
                            sokyhieu.Number = row["SOTHUTU"].ToString();
                            sokyhieu.Notation = row["KYHIEU"].ToString();
                            // loai van ban
                            INet.EdXml2.Header.Document loaivanban = new INet.EdXml2.Header.Document();
                            loaivanban.Name = row["LOAIVANBAN"].ToString();
                            loaivanban.Type = row["LOAIVANBAN"].ToString();
                            //content
                            INet.EdXml2.Header.Content noidung = new INet.EdXml2.Header.Content();
                            noidung.Value = CommonClass.GetString(Convert.FromBase64String(sending.NoiDung));
                            //subject
                            INet.EdXml2.Header.Subject trichyeu = new INet.EdXml2.Header.Subject();
                            trichyeu.Value = CommonClass.GetString(Convert.FromBase64String(sending.TrichYeu));
                            // ngay
                            INet.EdXml2.Header.ResponseDate ngayky = new INet.EdXml2.Header.ResponseDate();
                            //DateTime dngayky = DateTime.ParseExact(row["NGAYKY"].ToString().Replace("-", "/"), "dd/MM/yyyy", null);
                            //ngayky.Value = dngayky;

                            INet.EdXml2.Header.ResponseFor sngayky = new INet.EdXml2.Header.ResponseFor();
                            if (row["NGAYKY"] != null)
                            {
                                if (!string.IsNullOrEmpty(row["NGAYKY"].ToString()))
                                {
                                    DateTime sdngayky = DateTime.ParseExact(row["NGAYKY"].ToString().Replace("-", "/"), "dd/MM/yyyy", null);
                                    ngayky.Value = sdngayky;
                                }
                            }
                            //ngay ban hanh
                            INet.EdXml2.Header.Promulgation ngaybanhanh = new INet.EdXml2.Header.Promulgation();
                            if (row["NGAYBANHANH"] != null)
                            {
                                if (!string.IsNullOrEmpty(row["NGAYBANHANH"].ToString()))
                                {
                                    DateTime dngaybanhanh = DateTime.Now;
                                    try
                                    {
                                        dngaybanhanh = DateTime.ParseExact(row["NGAYBANHANH"].ToString().Replace("-", "/"), "dd/MM/yyyy", null);
                                    }
                                    catch (Exception)
                                    {
                                        dngaybanhanh = DateTime.Now;
                                    }
                                    ngaybanhanh.Date = dngaybanhanh;
                                    ngaybanhanh.Place = row["COQUANBANHANH"].ToString();
                                }
                            }
                            //thong tin khac
                            INet.EdXml2.Header.OtherInfo thongtinkhac = new INet.EdXml2.Header.OtherInfo();
                            //thongtinkhac.PageAmount = int.Parse("");
                            string sotrang = row["SOTRANG"].ToString();
                            if (string.IsNullOrEmpty(sotrang))
                            {
                                sotrang = "0";
                            }
                            string soban = row["SOBAN"].ToString();
                            if (string.IsNullOrEmpty(soban))
                            {
                                soban = "0";
                            }
                            string sobansaoluu = "";//row[""].ToString();
                            if (string.IsNullOrEmpty(sobansaoluu))
                            {
                                sobansaoluu = "0";
                            }
                            thongtinkhac.Priority = 0;
                            //thongtinkhac.Priority = int.Parse(row["DOMAT"].ToString());
                            thongtinkhac.PageAmount = int.Parse(sotrang);
                            thongtinkhac.PromulgationAmount = int.Parse(soban);
                            thongtinkhac.SphereOfPromulgation = sobansaoluu;

                            //dinh kem
                            INet.EdXml2.Attachment.FileAttach[] listatts = null;
                            if (!string.IsNullOrEmpty(sending.DinhKemFile))
                            {
                                foreach (string url in sending.DinhKemFile.Split(';'))
                                {
                                    if (!string.IsNullOrEmpty(url))
                                    {
                                        try
                                        {
                                            CookieAwareWebClient client = new CookieAwareWebClient(LotusConnection.cookies);
                                            client.DownloadFile(url, "temp\\" + Path.GetFileName(url));
                                            dinhkemname += Path.GetFileName(url) + ";";
                                            countattachments++;
                                        }
                                        catch
                                        {

                                        }
                                    }
                                }
                                //if (countattachments == 0)
                                //countattachments = 1;
                                listatts = new INet.EdXml2.Attachment.FileAttach[countattachments];
                                int addtoarray = 0;
                                foreach (string filename in dinhkemname.Split(';'))
                                {
                                    if (!string.IsNullOrEmpty(filename))
                                    {
                                        INet.EdXml2.Attachment.FileAttach att = new INet.EdXml2.Attachment.FileAttach();
                                        //att.ReadFile(Application.StartupPath + "\\temp\\" + filename, filename);
                                        string fpath = Application.StartupPath + "\\temp\\" + filename;
                                        att.ReadFile(fpath, filename);
                                        listatts[addtoarray] = att;
                                        addtoarray++;
                                    }
                                }
                            }
                            //noi nhan
                            string sendtoids = CommonClass.GetString(Convert.FromBase64String(sending.SendTo));
                            int countsendto = 0;
                            INet.EdXml2.Header.To[] dssendids = null;
                            if (!string.IsNullOrEmpty(sendtoids))
                            {
                                string[] senderId = null;
                                if (sendtoids.Contains(";"))
                                    senderId = sendtoids.Split(';');
                                else
                                    senderId = sendtoids.Split(',');
                                dssendids = new INet.EdXml2.Header.To[senderId.Length];
                                foreach (string sendto in senderId)
                                {
                                    if (!string.IsNullOrEmpty(sendto))
                                    {
                                        INet.EdXml2.Header.To t = new INet.EdXml2.Header.To();
                                        t.OrganId = sendto.Trim();
                                        DataRow[] rows = dtDsDonvi.Select("Code = '" + t.OrganId + "'");
                                        if (rows.Count() > 0 && rows != null)
                                        {
                                            t.OrganName = rows[0]["Name"].ToString();
                                        }
                                        dssendids[countsendto] = t;
                                    }
                                    countsendto++;
                                }
                            }

                            INet.EdXml2.Header.To[] listnoinhan = dssendids; //dssendids.ToArray();
                            //To[] listnoinhan = new To[1];
                            //To noinhan = new To();
                            //noinhan.OrganId = CommonClass.GetString(Convert.FromBase64String(sending.SendTo));
                            //noinhan.OrganName = row["TENNOINHAN"].ToString();
                            //listnoinhan[0] = noinhan;
                            //dong goi


                            docPack.From = noiguiden;
                            //docPack.Author = nguoiky;
                            docPack.SignerInfo = snguoiky;
                            docPack.Code = sokyhieu;
                            docPack.Document = loaivanban;
                            docPack.Subject = trichyeu;
                            //docPack.ResponseDate = ngayky;
                            docPack.ResponseFor = sngayky;
                            docPack.Promulgation = ngaybanhanh;
                            docPack.OtherInfo = thongtinkhac;
                            docPack.Content = noidung;
                            docPack.ToList = listnoinhan;
                            if (listatts == null)
                                listatts = new INet.EdXml2.Attachment.FileAttach[] { };
                            docPack.FileAttachList = listatts; //abc;
                            docPack.ToFile(Application.StartupPath + "\\temp\\" + sending.DocumentID + ".edxml");

                            Thread.Sleep(2000);
                            SendDoc(CommonClass.GetString(Convert.FromBase64String(sending.SendTo)), sending.DocumentID);
                            //xoa temp
                            CommonClass.DeleteFileFromTemp("temp\\" + sending.DocumentID + ".edxml");
                            if (!string.IsNullOrEmpty(sending.DinhKemFile))
                            {
                                foreach (string url in dinhkemname.Split(';'))//sending.DinhKemFile.Split(';'))
                                {
                                    if (!string.IsNullOrEmpty(url))
                                    {
                                        CommonClass.DeleteFileFromTemp("temp\\" + url);//Path.GetFileName(url));
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Gui van ban:::" + ex.StackTrace + ex.Message);
            }

            #region buildpack
            //EdXml docPack = new EdXml();
            ////nguoi ky
            //Author nguoiky = new Author();
            //nguoiky.FullName = "Cao Toàn Thành";
            //nguoiky.Function = "Giám đốc";
            //nguoiky.Competence = "Competence";
            ////so ky hieu
            //Code sokyhieu = new Code();
            //sokyhieu.Number = "298";
            //sokyhieu.Notation = "TDQD";
            //// loai van ban
            //Document loaivanban = new Document();
            //loaivanban.Name = "QD";
            //loaivanban.Type = "Quyết định";
            ////dinh kem
            ////List<FileAttach> abc = new List<FileAttach>();
            //FileAttach file = new FileAttach();
            //file.FileName = "dsada";
            ////content
            //Content noidung = new Content();
            //noidung.Value = "Nội dung văn bản";
            ////subject
            //Subject trichyeu = new Subject();
            //trichyeu.Value = "trích yếu văn bản";
            //FileAttach[] abc = new FileAttach[] { file };
            //docPack.Author = nguoiky;
            //docPack.Code = sokyhieu;
            //docPack.Document = loaivanban;
            //docPack.Subject = trichyeu;

            //file.FileStream = new MemoryStream(File.ReadAllBytes("knobstick.txt"));
            //docPack.Content = noidung;
            //docPack.FileAttachList = abc;
            //docPack.ToFile("guitd1.edxml");
            #endregion
        }
        private void createLogFile()
        {
            DataTable dt = new DataTable();
            if (File.Exists("GuiVB.xml"))
            {
                dt.ReadXml("GuiVB.xml");
            }
            dt.TableName = "Logs";

            //DataColumn dc = new DataColumn();
            //dc.ColumnName = "NgayGui";
            //dt.Columns.Add(dc);

            //dc = new DataColumn();
            //dc.ColumnName = "VanBan";
            //dt.Columns.Add(dc);

            //dc = new DataColumn();
            //dc.ColumnName = "TrichYeu";
            //dt.Columns.Add(dc);

            //dc = new DataColumn();
            //dc.ColumnName = "NoiNhan";
            //dt.Columns.Add(dc);

            //dc = new DataColumn();
            //dc.ColumnName = "TrangThai";
            //dt.Columns.Add(dc);

            DataRow r = dt.Rows.Add();
            r[0] = string.Empty;
            r[1] = string.Empty;
            r[2] = string.Empty;
            r[3] = string.Empty;
            r[4] = string.Empty;

            dt.WriteXml("GuiVB.xml");
            dt = null;
        }
        private bool ExtractEdxml(string packagename)
        {
            bool isSave = true;
            try
            {
                GetDocumentRecieve.Service docService = new GetDocumentRecieve.Service();
                GetLotusVersion.Service versionService = new GetLotusVersion.Service();
                TDOfficeService.MailSmsServicesService tdoffService = new TDOfficeService.MailSmsServicesService();
                string serverurl = CommonClass.TDVbden;
                if (!string.IsNullOrEmpty(CommonClass.TDFolder))
                {
                    serverurl = CommonClass.TDFolder + "/" + serverurl;
                }
                //EdXml a = new EdXml();
                INet.EdXml2.EdXml2 a = new INet.EdXml2.EdXml2();
                a.FromFile(packagename);
                INet.EdXml2.Attachment.FileAttach[] files = a.FileAttachList;
                string listfilename = string.Empty;
                string fieldname = string.Empty;
                string fieldvalue = string.Empty;
                //string dactavalue = CommonClass.DacTaVanBan(a.OtherInfo.PageAmount.ToString(), a.OtherInfo.Priority, a.Code.Number.ToString()
                //                        , a.OtherInfo.PromulgationAmount.ToString(), a.ResponseDate.Value.ToString(), a.Promulgation.Date.ToString(), a.Author.Competence, a.OtherInfo.Priority
                //                        , a.Promulgation.Place, "", "", a.Code.Number.ToString(), a.Code.Number.ToString(), a.Author.Function, "*" + a.Subject.Value
                //                        , "", a.Document.Name);
                string dactavalue = CommonClass.DacTaVanBan(thongtindonvi.AgencyResult.Code, a.OtherInfo.PageAmount.ToString(), a.OtherInfo.Priority.ToString(), a.Code.Number.ToString()
                                        , a.OtherInfo.PromulgationAmount.ToString(), a.ResponseFor.PromulgationDate.ToString(), a.Promulgation.Date.ToString("dd/MM/yyyy"), a.SignerInfo.Competence, a.OtherInfo.Priority.ToString()
                                        , a.Promulgation.Place, "", "", a.Code.Notation, a.Code.Number.ToString(), a.SignerInfo.Position, "*" + a.Subject.Value
                                        , "", a.Document.Name, a.From.OrganId);
                //a.Code.Notation - kyhieu
                DataTable dt = CommonClass.StringXmlToDataSet(dactavalue).Tables[0];
                DataColumnCollection cols = dt.Columns;
                DataRow row = dt.Rows[0];
                foreach (DataColumn col in cols)
                {
                    if (col != null)
                    {
                        string rename = col.ColumnName.ToUpper();
                        if (rename.Contains("coquanbanhanh".ToUpper()))
                            rename = "NOIGUI";
                        if (rename.ToUpper().Contains("VBLT"))
                        {
                            fieldname += rename + ";#";
                        }
                        else
                        {
                            fieldname += "STR" + rename + ";#";
                        }
                        fieldvalue += row[col.ColumnName].ToString() + ";#";
                    }
                }
                foreach (var file in files)
                {
                    file.WriteFile("temp\\" + file.OriginalName.Replace("\\", "-").Replace(";", "-").Replace("/", "-").Trim() + "." + file.FileExtention);
                    listfilename += file.OriginalName.Replace("\\", "-").Replace(";", "-").Replace("/", "-").Trim() + "." + file.FileExtention + ";";
                }
                int checkVersion;
                bool checkNum = int.TryParse(ExtractNumber(versionService.GetCurrentServerVersion("", "")[0].rMessage), out checkVersion);
                if (checkVersion > 6)
                {
                    string _newAttPath = string.Empty;
                    foreach (string _att in listfilename.Split(';'))
                    {
                        if (!string.IsNullOrEmpty(_att.Trim()))
                        {
                            try
                            {
                                byte[] attdata = File.ReadAllBytes("temp\\" + _att.Trim());
                                _newAttPath += tdoffService.uploadFileToServer(_att.Trim(), attdata) + ";";
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("Lai loi 2:" + ex.Message);
                            }
                        }
                    }
                    string ketqua = tdoffService.LuuEmailMoiLenServer(serverurl, CommonClass.StringToStringBase64(a.DocumentId.Value), CommonClass.StringToStringBase64("*" + a.Subject.Value), CommonClass.StringToStringBase64(a.From.OrganName), CommonClass.StringToStringBase64("*" + a.Content.Value), CommonClass.StringToStringBase64(String.Format("{0:MM/dd/yyyy HH:mm:ss}", DateTime.Now)), "", _newAttPath, CommonClass.StringToStringBase64(fieldname) + "###" + CommonClass.StringToStringBase64(fieldvalue));
                    if (ketqua == "1")
                    {
                        //MessageBox.Show("abc");
                    }
                }
                else
                {
                    foreach (string _att in listfilename.Split(';'))
                    {
                        if (!string.IsNullOrEmpty(_att.Trim()))
                        {
                            CommonClass.UploadFileToFtp("temp\\" + _att.Trim(), CommonClass.FtpServer, CommonClass.FtpUserAccount, CommonClass.FtpUserPassword);
                        }
                    }
                    GetDocumentRecieve.GuiSMS[] saveDoc = docService.LuuEmailMoiLenServer(serverurl, CommonClass.StringToStringBase64(a.DocumentId.Value), CommonClass.StringToStringBase64("*" + a.Subject.Value), CommonClass.StringToStringBase64(a.From.OrganName), CommonClass.StringToStringBase64("*" + a.Content.Value), CommonClass.StringToStringBase64(String.Format("{0:MM/dd/yyyy HH:mm:ss}", DateTime.Now)), CommonClass.FtpServer, listfilename, CommonClass.StringToStringBase64(fieldname) + "###" + CommonClass.StringToStringBase64(fieldvalue));
                }
                foreach (string _att in listfilename.Split(';'))
                {
                    if (!string.IsNullOrEmpty(_att.Trim()))
                    {
                        if (checkVersion <= 6)
                            CommonClass.DeleteFileFromFtp(CommonClass.FtpServer, _att.Trim(), CommonClass.FtpUserAccount, CommonClass.FtpUserPassword);
                       //CommonClass.DeleteFileFromTemp("temp\\" + _att.Trim());
                    }
                }
                CommonClass.DeleteFileFromTemp(packagename);
                docService.Dispose();
                versionService.Dispose();
                tdoffService.Dispose();
                isSave = true;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.StackTrace);
                File.WriteAllText("C:\\logs.txt", ex.Message);
                isSave = false;
                return isSave;
            }
            return isSave;
        }
        private void ConnectToServer()
        {
            lotus = new LotusConnection(txtTDUrl.Text.Trim(), 8083, txtTDUser.Text.Trim(), txtTDPw.Text.Trim());
            if (lotus.Init())
            {
                for (int i = 1; i < 100; i++)
                {
                    bgTransferVB.ReportProgress(i);
                    if (i == 50)
                    {
                        Thread.Sleep(2000);
                        IsConnectedToServer = true;
                        btnTDLogin.Enabled = false;
                    }
                    if (i == 70)
                    {
                        Thread.Sleep(2000);
                    }
                    if (i == 99)
                    {
                        Thread.Sleep(2000);
                        txtTDUrl.Enabled = false;
                        txtTDPw.Enabled = false;
                        txtTDUser.Enabled = false;
                        btnTDDisConnect.Enabled = true;
                        groupLienThong.Enabled = true;
                        toolStatus.Text = "Đăng nhập hệ thống thành công.";
                    }
                }
            }
            else
            {
                toolStatus.Text = "Đăng nhập thất bại. Kiểm tra lại thông tin.";
            }
        }
        private void DisconnectServer()
        {
            if (IsConnectedToServer == true)
            {
                for (int i = 1; i < 100; i++)
                {
                    bgTransferVB.ReportProgress(i);
                    if (i == 50)
                    {
                        LotusConnection.Close(txtTDUrl.Text.Trim());
                        Thread.Sleep(2000);
                    }
                    if (i == 70)
                    {
                        Thread.Sleep(2000);
                    }
                    if (i == 99)
                    {
                        txtTDUrl.Enabled = true;
                        txtTDPw.Enabled = true;
                        txtTDUser.Enabled = true;
                        btnTDLogin.Enabled = true;
                        btnTDDisConnect.Enabled = false;
                        groupLienThong.Enabled = false;
                        IsConnectedToServer = false;
                        hcmservice.Dispose();
                        toolStatus.Text = "Đăng xuất khỏi hệ thống.";
                    }
                }
            }
        }
        private void BeginSendAndRecieve()
        {
            toolStatus.Text = "Bắt đầu gửi nhận văn bản";
            isC = CommonClass.Connected(txtApiUrl.Text.Trim());
            for (int i = 1; i < 100; i++)
            {
                bgTransferVB.ReportProgress(i);
                if (i == 30)
                {
                    if (timer == null)
                    {
                        double setTime = CommonClass.ConvertMinutesToMilliseconds(5);
                        if (!string.IsNullOrEmpty(txtTime.Text))
                            setTime = CommonClass.ConvertMinutesToMilliseconds(double.Parse(txtTime.Text));
                        timer = new System.Timers.Timer(setTime);
                        this.timer.AutoReset = true;
                        this.timer.Elapsed += new System.Timers.ElapsedEventHandler(this.timer_Elapsed);
                        this.timer.Start();
                        txtApiUrl.Enabled = false;
                        txtTaiKhoan.Enabled = false;
                        txtMatKhau.Enabled = false;
                        btnRecieve.Enabled = false;
                        btnSend.Enabled = true;
                        Thread.Sleep(2000);
                    }
                    else
                    {
                        hcmservice = IWSClientFactory.CreateMercuryService(txtTaiKhoan.Text, txtMatKhau.Text, hcmconfig);
                    }
                }
                if (i == 40)
                {
                    toolStatus.Text = "Đang gửi văn bản ...";
                    BuildDocPackage();
                    Thread.Sleep(2000);
                }
                if (i == 60)
                {
                    toolStatus.Text = "Đang nhận văn bản ...";
                    RecieveDoc();
                    Thread.Sleep(2000);
                }

                if (i == 80)
                {
                    toolStatus.Text = "Đang cập nhật trạng thái ...";
                    CapNhatTrangThai();
                    Thread.Sleep(2000);
                }

                if (i == 99)
                {
                    DeleteSpamFile();
                    Thread.Sleep(2000);
                }
            }
            toolStatus.Text = "Hoàn thành gửi nhận văn bản.";
            hcmservice.Dispose();
        }
        private void btnSend_Click(object sender, EventArgs e)
        {
            if (timer != null)
            {
                toolStatusProcess.Value = 0;
                timer.Stop();
                timer = null;
                bgTransferVB.CancelAsync();
                bgTransferVB.Dispose();
                txtApiUrl.Enabled = true;
                txtTaiKhoan.Enabled = true;
                txtMatKhau.Enabled = true;
                btnRecieve.Enabled = true;
                btnSend.Enabled = false;
                toolStatus.Text = "Dừng gửi nhận văn bản.";
            }
            //BuildDocPackage();
        }
        private void btnRecieve_Click(object sender, EventArgs e)
        {
            try
            {
                hcmconfig = new MercuryServiceConfig();
                hcmconfig.ServiceUrl = txtApiUrl.Text;
                hcmconfig.ApplicationName = CommonClass.TDApplicationName;
                hcmservice = IWSClientFactory.CreateMercuryService(txtTaiKhoan.Text, txtMatKhau.Text, hcmconfig);

                //thong tin cua don vi dang su dung phan mem lay van ban
                thongtindonvi = hcmservice.GetAgency();
                dtDsDonvi = DanhSachNguoiNhan();
                checktimerwork = "run";
                bgTransferVB.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void btnTDLogin_Click(object sender, EventArgs e)
        {
            checktimerwork = "connected";
            bgTransferVB.RunWorkerAsync();
        }
        private void btnTDDisConnect_Click(object sender, EventArgs e)
        {
            try
            {
                checktimerwork = "disconnected";
                bgTransferVB.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Không thể dừng chương trình. Thử lại sau.");
            }
        }
        private void btnSaveConfig_Click(object sender, EventArgs e)
        {
            //string[] keyname = new string[] { "HcmServerUrl", "AccessKey", "SecretKey", "ApplicationName", 
            //                                  "FtpServer", "FtpUsername", "FtpPassword", "TDServerUrl", "TDVbdi"
            //                                  , "TDVbden", "TDUsername", "TDPassword"};
            string[] keyname = new string[] { "HcmServerUrl", "AccessKey", "SecretKey", 
                                              "FtpServer", "FtpUsername", "FtpPassword", "TDServerUrl", "TDVbdi"
                                              , "TDVbden", "TDUsername", "TDPassword", "TDFolder", "ApplicationName"};
            CommonClass.SaveAppSettingsKeyValueInAppConfig(keyname[0].ToString(), txtApiUrl.Text.Trim());
            CommonClass.SaveAppSettingsKeyValueInAppConfig(keyname[1].ToString(), tdKey.TDEncrypt(txtTaiKhoan.Text));
            CommonClass.SaveAppSettingsKeyValueInAppConfig(keyname[2].ToString(), tdKey.TDEncrypt(txtMatKhau.Text));
            CommonClass.SaveAppSettingsKeyValueInAppConfig(keyname[3].ToString(), txtFtpUrl.Text);
            CommonClass.SaveAppSettingsKeyValueInAppConfig(keyname[4].ToString(), txtFtpUsername.Text);
            CommonClass.SaveAppSettingsKeyValueInAppConfig(keyname[5].ToString(), tdKey.TDEncrypt(txtFtpPassword.Text));
            CommonClass.SaveAppSettingsKeyValueInAppConfig(keyname[6].ToString(), txtTDUrl.Text);//tdKey.TDEncrypt(txtTaiKhoan.Text));
            CommonClass.SaveAppSettingsKeyValueInAppConfig(keyname[7].ToString(), txtVBGui.Text);//tdKey.TDEncrypt(txtTaiKhoan.Text));
            CommonClass.SaveAppSettingsKeyValueInAppConfig(keyname[8].ToString(), txtVBNhan.Text); //tdKey.TDEncrypt(txtMatKhau.Text));
            CommonClass.SaveAppSettingsKeyValueInAppConfig(keyname[9].ToString(), txtTDUser.Text);//tdKey.TDEncrypt(txtFtpPassword.Text));
            CommonClass.SaveAppSettingsKeyValueInAppConfig(keyname[10].ToString(), tdKey.TDEncrypt(txtTDPw.Text));
            CommonClass.SaveAppSettingsKeyValueInAppConfig(keyname[11].ToString(), txtFolder.Text);
            CommonClass.SaveAppSettingsKeyValueInAppConfig(keyname[12].ToString(), txtApplicationName.Text);
            string serviceurl = txtTDUrl.Text + "/" + txtServices.Text;
            if (string.IsNullOrEmpty(txtServices.Text))
            {
                serviceurl = txtTDUrl.Text + txtServices.Text;
            }
            CommonClass.SaveApplicationSettings(serviceurl);
            DialogResult _r = MessageBox.Show("Chương trình cần khởi động lại để cập nhật thay đổi?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk);
            if (_r == DialogResult.Yes)
            {
                Application.Exit();
                Process.Start(Application.ExecutablePath, "/restart" + Process.GetCurrentProcess().Id);
            }
        }
        private void bgTransferVB_DoWork(object sender, DoWorkEventArgs e)
        {
            if (checktimerwork == "connected")
            {
                ConnectToServer();
            }
            if (checktimerwork == "disconnected")
            {
                DisconnectServer();
            }
            if (checktimerwork == "run")
            {
                BeginSendAndRecieve();
            }
            //for (int i = 1; i < 100; i++)
            //{
            //    bgTransferVB.ReportProgress(i);
            //    if (i == 50)
            //    {
            //        Thread.Sleep(2000);
            //    }
            //    if (i == 70)
            //    {
            //        Thread.Sleep(2000);
            //    }
            //    if (i == 100)
            //    {
            //        Thread.Sleep(2000);
            //    }
            //    if (bgTransferVB.CancellationPending)
            //    {
            //        e.Cancel = true;
            //        bgTransferVB.ReportProgress(0);
            //        return;
            //    }
            //}
        }
        private void bgTransferVB_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            toolStatusProcess.Value = e.ProgressPercentage;
        }
        private void bgTransferVB_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {

            }
            else if (e.Error != null)
            {
                bgTransferVB.CancelAsync();
                bgTransferVB.Dispose();
            }
            else
            {
                toolStatusProcess.Value = 0;
                bgTransferVB.Dispose();
            }
        }
        private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            bgTransferVB.RunWorkerAsync();
        }
        private void txtMatKhau_TextChanged(object sender, EventArgs e)
        {

        }
        private void txtTime_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtTime.Text))
            {
                int checktime = int.Parse(txtTime.Text);
                if (checktime > 60)
                {
                    txtTime.Text = "60";
                }
            }
        }
        private void txtTime_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar) && !char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }
        private string ExtractNumber(string toExtract)
        {
            string resultString = System.Text.RegularExpressions.Regex.Match(toExtract, @"\d+").Value;
            return resultString;
        }
        private void btnCapNhatDonVi_Click(object sender, EventArgs e)
        {
            GetListOfOrgans();
        }
        private void hcm_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                this.Hide();
                notifyTray.Visible = true;
                notifyTray.ShowBalloonTip(500, "Thông báo", "Liên thông văn bản", ToolTipIcon.Info);
            }
        }
        private void notifyTray_MouseClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
            notifyTray.Visible = false;
        }
        private void notifyTray_Click(object sender, EventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
            notifyTray.Visible = false;
        }
        private void notifyTray_BalloonTipClicked(object sender, EventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
            notifyTray.Visible = false;
        }
        private void hcm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                notifyTray.Visible = true;
                notifyTray.ShowBalloonTip(500, "Thông báo", "Liên thông văn bản", ToolTipIcon.Info);
                this.Hide();
                e.Cancel = true;
            }
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void btnBrowser_Click(object sender, EventArgs e)
        {
            OpenFileDialog fDialog = new OpenFileDialog();
            fDialog.Title = "Chọn tệp văn bản liên thông";
            fDialog.Filter = "Văn bản|*.edxml";
            if (fDialog.ShowDialog() == DialogResult.OK)
            {
                txtFilePath.Text = fDialog.FileName.ToString();
            }
        }
        private void btnUpVB_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtFilePath.Text))
                ExtractEdxml(txtFilePath.Text);
            //try
            //{
            //    bool isSave = true;
            //    try
            //    {
            //        //EdXml a = new EdXml();
            //        INet.EdXml2.EdXml2 a = new INet.EdXml2.EdXml2();
            //        a.FromFile(txtFilePath.Text);
            //        INet.EdXml2.Attachment.FileAttach[] files = a.FileAttachList;
            //        string listfilename = string.Empty;
            //        string fieldname = string.Empty;
            //        string fieldvalue = string.Empty;
            //        string dactavalue = CommonClass.DacTaVanBan(a.OtherInfo.PageAmount.ToString(), a.OtherInfo.Priority, a.Code.Number.ToString()
            //                                , a.OtherInfo.PromulgationAmount.ToString(), a.ResponseDate.Value.ToString(), a.Promulgation.Date.ToString(), a.Author.Competence, a.OtherInfo.Priority
            //                                , a.Promulgation.Place, "", "", a.Code.Number.ToString(), a.Code.Number.ToString(), a.Author.Function, "*" + a.Subject.Value
            //                                , "", a.Document.Name);
            //        //a.Code.Notation - kyhieu
            //        DataTable dt = CommonClass.StringXmlToDataSet(dactavalue).Tables[0];
            //        DataColumnCollection cols = dt.Columns;
            //        DataRow row = dt.Rows[0];
            //        foreach (DataColumn col in cols)
            //        {
            //            if (col != null)
            //            {
            //                string rename = col.ColumnName.ToUpper();
            //                if (rename.Contains("coquanbanhanh".ToUpper()))
            //                    rename = "NOIGUI";
            //                fieldname += "STR" + rename + ";#";
            //                fieldvalue += row[col.ColumnName].ToString() + ";#";
            //            }
            //        }
            //        foreach (var file in files)
            //        {
            //            file.WriteFile("temp\\" + file.OriginalName.Replace("\\", "-").Replace(";", "-").Replace("/", "-").Trim() + "." + file.FileExtention);
            //            listfilename += file.OriginalName.Replace("\\", "-").Replace(";", "-").Replace("/", "-").Trim() + "." + file.FileExtention + ";";
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        MessageBox.Show(ex.StackTrace + ex.Message);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show( ex.Message);
            //}
        }

        private void CreateLogFile(string logFilePath, string logfilename)
        {
            if (File.Exists(logFilePath + logfilename + ".xml"))
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(logFilePath + logfilename + ".xml");
                InsertLog(doc);
                doc.Save(logFilePath + logfilename + ".xml");
            }
            else
            {
                try
                {
                    XmlDocument doc = new XmlDocument();
                    XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
                    doc.AppendChild(docNode);
                    XmlNode productsNode = doc.CreateElement("settings");
                    doc.AppendChild(productsNode);
                    InsertLog(doc);
                    doc.Save(logFilePath + logfilename + ".xml");
                }
                catch (Exception ex)
                {

                }
            }
        }
        private void InsertLog(XmlDocument doc)
        {
            XmlNode productNode = doc.CreateElement("setting");
            doc.DocumentElement.AppendChild(productNode);
            XmlNode timeStart = doc.CreateElement("timeStart");
            timeStart.InnerText = logs.TimeStart;
            productNode.AppendChild(timeStart);
            XmlNode startWithServerName = doc.CreateElement("startWithServerName");
            startWithServerName.InnerText = logs.StartWithServerName;
            productNode.AppendChild(startWithServerName);
            XmlNode isConnectedServer = doc.CreateElement("isConnectedServer");
            isConnectedServer.InnerText = logs.IsConnectedServer;
            productNode.AppendChild(isConnectedServer);
            XmlNode organizationName = doc.CreateElement("organizationName");
            organizationName.InnerText = logs.OrganizationName;
            productNode.AppendChild(organizationName);
            XmlNode organizationDbPath = doc.CreateElement("organizationDbPath");
            organizationDbPath.InnerText = logs.OrganizationDbPath;
            productNode.AppendChild(organizationDbPath);

            XmlNode sendDocumentType = doc.CreateElement("sendDocumentType");
            sendDocumentType.InnerText = logs.SendDocumentType;
            productNode.AppendChild(sendDocumentType);

            XmlNode organizationEmailAdd = doc.CreateElement("organizationEmailAdd");
            organizationEmailAdd.InnerText = logs.OrganizationEmailAdd;
            productNode.AppendChild(organizationEmailAdd);
            XmlNode isEmailConnected = doc.CreateElement("isEmailConnected");
            isEmailConnected.InnerText = logs.IsEmailConnected;
            productNode.AppendChild(isEmailConnected);
            XmlNode errorAttachment = doc.CreateElement("errorAttachment");
            errorAttachment.InnerText = logs.ErrorAttachment;
            productNode.AppendChild(errorAttachment);
            XmlNode saveMailResult = doc.CreateElement("saveMailResult");
            saveMailResult.InnerText = logs.SaveMailResult;
            productNode.AppendChild(saveMailResult);
            XmlNode haveNewEmail = doc.CreateElement("haveNewEmail");
            haveNewEmail.InnerText = logs.HaveNewEmail;
            productNode.AppendChild(haveNewEmail);

            XmlNode emailSent = doc.CreateElement("emailSent");
            emailSent.InnerText = logs.EmailSent;
            productNode.AppendChild(emailSent);

            XmlNode emailNotSent = doc.CreateElement("emailNotSent");
            emailNotSent.InnerText = logs.EmailNotSent;
            productNode.AppendChild(emailNotSent);

            XmlNode isEnd = doc.CreateElement("isEnd");
            isEnd.InnerText = logs.IsEnd;
            productNode.AppendChild(isEnd);
        }
    }

    public class LogVanBan
    {
        string timeStart;

        public string TimeStart
        {
            get { return timeStart; }
            set { timeStart = value; }
        }
        string startWithServerName;

        public string StartWithServerName
        {
            get { return startWithServerName; }
            set { startWithServerName = value; }
        }
        string isConnectedServer;

        public string IsConnectedServer
        {
            get { return isConnectedServer; }
            set { isConnectedServer = value; }
        }
        string organizationName;

        public string OrganizationName
        {
            get { return organizationName; }
            set { organizationName = value; }
        }
        string organizationEmailAdd;

        public string OrganizationEmailAdd
        {
            get { return organizationEmailAdd; }
            set { organizationEmailAdd = value; }
        }
        string isEmailConnected;

        public string IsEmailConnected
        {
            get { return isEmailConnected; }
            set { isEmailConnected = value; }
        }
        string errorAttachment;

        public string ErrorAttachment
        {
            get { return errorAttachment; }
            set { errorAttachment = value; }
        }
        string saveMailResult;

        public string SaveMailResult
        {
            get { return saveMailResult; }
            set { saveMailResult = value; }
        }
        string haveNewEmail;

        public string HaveNewEmail
        {
            get { return haveNewEmail; }
            set { haveNewEmail = value; }
        }
        string isEnd;

        public string IsEnd
        {
            get { return isEnd; }
            set { isEnd = value; }
        }
        string organizationDbPath;

        public string OrganizationDbPath
        {
            get { return organizationDbPath; }
            set { organizationDbPath = value; }
        }
        string sendDocumentType;

        public string SendDocumentType
        {
            get { return sendDocumentType; }
            set { sendDocumentType = value; }
        }
        string emailSent;

        public string EmailSent
        {
            get { return emailSent; }
            set { emailSent = value; }
        }
        string emailNotSent;

        public string EmailNotSent
        {
            get { return emailNotSent; }
            set { emailNotSent = value; }
        }
    }
}