﻿namespace DemoBTT
{
    partial class hcm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(hcm));
            this.txtMatKhau = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTaiKhoan = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSend = new System.Windows.Forms.Button();
            this.btnRecieve = new System.Windows.Forms.Button();
            this.txtApiUrl = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupDomino = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTDUrl = new System.Windows.Forms.TextBox();
            this.btnTDLogin = new System.Windows.Forms.Button();
            this.btnTDDisConnect = new System.Windows.Forms.Button();
            this.txtTDPw = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtTDUser = new System.Windows.Forms.TextBox();
            this.groupLienThong = new System.Windows.Forms.GroupBox();
            this.btnCapNhatDonVi = new System.Windows.Forms.Button();
            this.statusLienThong = new System.Windows.Forms.StatusStrip();
            this.toolStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStatusProcess = new System.Windows.Forms.ToolStripProgressBar();
            this.bgTransferVB = new System.ComponentModel.BackgroundWorker();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtTime = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtServices = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtFolder = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtVBGui = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtVBNhan = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtFtpUrl = new System.Windows.Forms.TextBox();
            this.txtFtpPassword = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtFtpUsername = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btnUpVB = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.txtFilePath = new System.Windows.Forms.TextBox();
            this.btnBrowser = new System.Windows.Forms.Button();
            this.btnSaveConfig = new System.Windows.Forms.Button();
            this.notifyTray = new System.Windows.Forms.NotifyIcon(this.components);
            this.btnExit = new System.Windows.Forms.Button();
            this.btnLogs = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.txtApplicationName = new System.Windows.Forms.TextBox();
            this.groupDomino.SuspendLayout();
            this.groupLienThong.SuspendLayout();
            this.statusLienThong.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtMatKhau
            // 
            this.txtMatKhau.Location = new System.Drawing.Point(91, 80);
            this.txtMatKhau.Name = "txtMatKhau";
            this.txtMatKhau.Size = new System.Drawing.Size(227, 20);
            this.txtMatKhau.TabIndex = 12;
            this.txtMatKhau.Text = "A8PWjHTDMtnnHX5rAIE2nvvixqLQOh9nKAeqzE5eHx5s";
            this.txtMatKhau.UseSystemPasswordChar = true;
            this.txtMatKhau.TextChanged += new System.EventHandler(this.txtMatKhau_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "SecretKey";
            // 
            // txtTaiKhoan
            // 
            this.txtTaiKhoan.Location = new System.Drawing.Point(91, 54);
            this.txtTaiKhoan.Name = "txtTaiKhoan";
            this.txtTaiKhoan.Size = new System.Drawing.Size(227, 20);
            this.txtTaiKhoan.TabIndex = 10;
            this.txtTaiKhoan.Text = "522ed15ae4b0a0ffef088576";
            this.txtTaiKhoan.UseSystemPasswordChar = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "AccessKey";
            // 
            // btnSend
            // 
            this.btnSend.Enabled = false;
            this.btnSend.Location = new System.Drawing.Point(162, 106);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(75, 23);
            this.btnSend.TabIndex = 8;
            this.btnSend.Text = "Dừng";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // btnRecieve
            // 
            this.btnRecieve.Location = new System.Drawing.Point(243, 106);
            this.btnRecieve.Name = "btnRecieve";
            this.btnRecieve.Size = new System.Drawing.Size(75, 23);
            this.btnRecieve.TabIndex = 7;
            this.btnRecieve.Text = "Bắt đầu";
            this.btnRecieve.UseVisualStyleBackColor = true;
            this.btnRecieve.Click += new System.EventHandler(this.btnRecieve_Click);
            // 
            // txtApiUrl
            // 
            this.txtApiUrl.Location = new System.Drawing.Point(91, 28);
            this.txtApiUrl.Name = "txtApiUrl";
            this.txtApiUrl.Size = new System.Drawing.Size(227, 20);
            this.txtApiUrl.TabIndex = 14;
            this.txtApiUrl.Text = "http://imercury.inetcloud.vn/ihorae";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "API Url";
            // 
            // groupDomino
            // 
            this.groupDomino.Controls.Add(this.label4);
            this.groupDomino.Controls.Add(this.txtTDUrl);
            this.groupDomino.Controls.Add(this.btnTDLogin);
            this.groupDomino.Controls.Add(this.btnTDDisConnect);
            this.groupDomino.Controls.Add(this.txtTDPw);
            this.groupDomino.Controls.Add(this.label5);
            this.groupDomino.Controls.Add(this.label6);
            this.groupDomino.Controls.Add(this.txtTDUser);
            this.groupDomino.Location = new System.Drawing.Point(8, 6);
            this.groupDomino.Name = "groupDomino";
            this.groupDomino.Size = new System.Drawing.Size(333, 131);
            this.groupDomino.TabIndex = 15;
            this.groupDomino.TabStop = false;
            this.groupDomino.Text = "Thông tin truy cập hệ thống TDOFFICE";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "Server Url";
            // 
            // txtTDUrl
            // 
            this.txtTDUrl.Location = new System.Drawing.Point(93, 22);
            this.txtTDUrl.Name = "txtTDUrl";
            this.txtTDUrl.Size = new System.Drawing.Size(227, 20);
            this.txtTDUrl.TabIndex = 22;
            this.txtTDUrl.Text = "http://192.168.2.206:8083";
            // 
            // btnTDLogin
            // 
            this.btnTDLogin.Location = new System.Drawing.Point(245, 100);
            this.btnTDLogin.Name = "btnTDLogin";
            this.btnTDLogin.Size = new System.Drawing.Size(75, 23);
            this.btnTDLogin.TabIndex = 15;
            this.btnTDLogin.Text = "Connect";
            this.btnTDLogin.UseVisualStyleBackColor = true;
            this.btnTDLogin.Click += new System.EventHandler(this.btnTDLogin_Click);
            // 
            // btnTDDisConnect
            // 
            this.btnTDDisConnect.Enabled = false;
            this.btnTDDisConnect.Location = new System.Drawing.Point(164, 100);
            this.btnTDDisConnect.Name = "btnTDDisConnect";
            this.btnTDDisConnect.Size = new System.Drawing.Size(75, 23);
            this.btnTDDisConnect.TabIndex = 16;
            this.btnTDDisConnect.Text = "Disconnect";
            this.btnTDDisConnect.UseVisualStyleBackColor = true;
            this.btnTDDisConnect.Click += new System.EventHandler(this.btnTDDisConnect_Click);
            // 
            // txtTDPw
            // 
            this.txtTDPw.Location = new System.Drawing.Point(93, 74);
            this.txtTDPw.Name = "txtTDPw";
            this.txtTDPw.Size = new System.Drawing.Size(227, 20);
            this.txtTDPw.TabIndex = 20;
            this.txtTDPw.Text = "1";
            this.txtTDPw.UseSystemPasswordChar = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 51);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Username";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 77);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "Password";
            // 
            // txtTDUser
            // 
            this.txtTDUser.Location = new System.Drawing.Point(93, 48);
            this.txtTDUser.Name = "txtTDUser";
            this.txtTDUser.Size = new System.Drawing.Size(227, 20);
            this.txtTDUser.TabIndex = 18;
            this.txtTDUser.Text = "quantri";
            // 
            // groupLienThong
            // 
            this.groupLienThong.Controls.Add(this.btnCapNhatDonVi);
            this.groupLienThong.Controls.Add(this.label3);
            this.groupLienThong.Controls.Add(this.txtApiUrl);
            this.groupLienThong.Controls.Add(this.btnRecieve);
            this.groupLienThong.Controls.Add(this.btnSend);
            this.groupLienThong.Controls.Add(this.txtMatKhau);
            this.groupLienThong.Controls.Add(this.label1);
            this.groupLienThong.Controls.Add(this.label2);
            this.groupLienThong.Controls.Add(this.txtTaiKhoan);
            this.groupLienThong.Enabled = false;
            this.groupLienThong.Location = new System.Drawing.Point(8, 143);
            this.groupLienThong.Name = "groupLienThong";
            this.groupLienThong.Size = new System.Drawing.Size(333, 135);
            this.groupLienThong.TabIndex = 16;
            this.groupLienThong.TabStop = false;
            this.groupLienThong.Text = "Thông tin hệ thống liên thông";
            // 
            // btnCapNhatDonVi
            // 
            this.btnCapNhatDonVi.Location = new System.Drawing.Point(65, 106);
            this.btnCapNhatDonVi.Name = "btnCapNhatDonVi";
            this.btnCapNhatDonVi.Size = new System.Drawing.Size(91, 23);
            this.btnCapNhatDonVi.TabIndex = 15;
            this.btnCapNhatDonVi.Text = "Cập nhật đơn vị";
            this.btnCapNhatDonVi.UseVisualStyleBackColor = true;
            this.btnCapNhatDonVi.Click += new System.EventHandler(this.btnCapNhatDonVi_Click);
            // 
            // statusLienThong
            // 
            this.statusLienThong.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStatus,
            this.toolStatusProcess});
            this.statusLienThong.Location = new System.Drawing.Point(0, 341);
            this.statusLienThong.Name = "statusLienThong";
            this.statusLienThong.Size = new System.Drawing.Size(355, 22);
            this.statusLienThong.SizingGrip = false;
            this.statusLienThong.TabIndex = 17;
            this.statusLienThong.Text = "statusStrip1";
            // 
            // toolStatus
            // 
            this.toolStatus.Name = "toolStatus";
            this.toolStatus.Size = new System.Drawing.Size(160, 17);
            this.toolStatus.Text = "Hệ thống liên thông Văn bản";
            // 
            // toolStatusProcess
            // 
            this.toolStatusProcess.Name = "toolStatusProcess";
            this.toolStatusProcess.Size = new System.Drawing.Size(100, 16);
            this.toolStatusProcess.Step = 1;
            this.toolStatusProcess.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            // 
            // bgTransferVB
            // 
            this.bgTransferVB.WorkerReportsProgress = true;
            this.bgTransferVB.WorkerSupportsCancellation = true;
            this.bgTransferVB.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgTransferVB_DoWork);
            this.bgTransferVB.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgTransferVB_ProgressChanged);
            this.bgTransferVB.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgTransferVB_RunWorkerCompleted);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(355, 315);
            this.tabControl1.TabIndex = 18;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupDomino);
            this.tabPage1.Controls.Add(this.groupLienThong);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(347, 289);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Cấu hình Server";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Controls.Add(this.groupBox2);
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(347, 289);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Cấu hình khác";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.txtApplicationName);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.txtTime);
            this.groupBox3.Location = new System.Drawing.Point(8, 196);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(333, 87);
            this.groupBox3.TabIndex = 23;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Thông tin khác";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(144, 20);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(28, 13);
            this.label13.TabIndex = 2;
            this.label13.Text = "phút";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 20);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(95, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Thời gian gửi nhận";
            // 
            // txtTime
            // 
            this.txtTime.Location = new System.Drawing.Point(110, 17);
            this.txtTime.MaxLength = 2;
            this.txtTime.Name = "txtTime";
            this.txtTime.Size = new System.Drawing.Size(28, 20);
            this.txtTime.TabIndex = 0;
            this.txtTime.TextChanged += new System.EventHandler(this.txtTime_TextChanged);
            this.txtTime.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTime_KeyPress);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.txtServices);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.txtFolder);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.txtVBGui);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.txtVBNhan);
            this.groupBox2.Location = new System.Drawing.Point(3, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(333, 75);
            this.groupBox2.TabIndex = 23;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Thông tin lưu văn bản gửi nhận";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 48);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(48, 13);
            this.label15.TabIndex = 25;
            this.label15.Text = "Services";
            // 
            // txtServices
            // 
            this.txtServices.Location = new System.Drawing.Point(61, 45);
            this.txtServices.Name = "txtServices";
            this.txtServices.Size = new System.Drawing.Size(98, 20);
            this.txtServices.TabIndex = 26;
            this.txtServices.Text = "ctthanh";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 22);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(49, 13);
            this.label14.TabIndex = 23;
            this.label14.Text = "Thư mục";
            // 
            // txtFolder
            // 
            this.txtFolder.Location = new System.Drawing.Point(61, 19);
            this.txtFolder.Name = "txtFolder";
            this.txtFolder.Size = new System.Drawing.Size(98, 20);
            this.txtFolder.TabIndex = 24;
            this.txtFolder.Text = "hanam";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(165, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(40, 13);
            this.label10.TabIndex = 21;
            this.label10.Text = "VB Gửi";
            // 
            // txtVBGui
            // 
            this.txtVBGui.Location = new System.Drawing.Point(212, 19);
            this.txtVBGui.Name = "txtVBGui";
            this.txtVBGui.Size = new System.Drawing.Size(111, 20);
            this.txtVBGui.TabIndex = 22;
            this.txtVBGui.Text = "hanam/vbdi.nsf";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(162, 48);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(50, 13);
            this.label11.TabIndex = 17;
            this.label11.Text = "VB Nhận";
            // 
            // txtVBNhan
            // 
            this.txtVBNhan.Location = new System.Drawing.Point(212, 45);
            this.txtVBNhan.Name = "txtVBNhan";
            this.txtVBNhan.Size = new System.Drawing.Size(111, 20);
            this.txtVBNhan.TabIndex = 18;
            this.txtVBNhan.Text = "hanam/vbden.nsf";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtFtpUrl);
            this.groupBox1.Controls.Add(this.txtFtpPassword);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txtFtpUsername);
            this.groupBox1.Location = new System.Drawing.Point(5, 87);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(333, 103);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin FTP";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "FTP Url";
            // 
            // txtFtpUrl
            // 
            this.txtFtpUrl.Location = new System.Drawing.Point(93, 22);
            this.txtFtpUrl.Name = "txtFtpUrl";
            this.txtFtpUrl.Size = new System.Drawing.Size(227, 20);
            this.txtFtpUrl.TabIndex = 22;
            this.txtFtpUrl.Text = "ftp://192.168.2.7/Khach_Hang(Anonymous)/ctthanh-dungcoxoa/";
            // 
            // txtFtpPassword
            // 
            this.txtFtpPassword.Location = new System.Drawing.Point(93, 74);
            this.txtFtpPassword.Name = "txtFtpPassword";
            this.txtFtpPassword.Size = new System.Drawing.Size(227, 20);
            this.txtFtpPassword.TabIndex = 20;
            this.txtFtpPassword.Text = "thanhcao";
            this.txtFtpPassword.UseSystemPasswordChar = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 51);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Username";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 77);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "Password";
            // 
            // txtFtpUsername
            // 
            this.txtFtpUsername.Location = new System.Drawing.Point(93, 48);
            this.txtFtpUsername.Name = "txtFtpUsername";
            this.txtFtpUsername.Size = new System.Drawing.Size(227, 20);
            this.txtFtpUsername.TabIndex = 18;
            this.txtFtpUsername.Text = "tandan\\ctthanh";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.btnUpVB);
            this.tabPage3.Controls.Add(this.label16);
            this.tabPage3.Controls.Add(this.txtFilePath);
            this.tabPage3.Controls.Add(this.btnBrowser);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(347, 289);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Mở rộng";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // btnUpVB
            // 
            this.btnUpVB.Location = new System.Drawing.Point(255, 61);
            this.btnUpVB.Name = "btnUpVB";
            this.btnUpVB.Size = new System.Drawing.Size(75, 23);
            this.btnUpVB.TabIndex = 3;
            this.btnUpVB.Text = "Tải lên";
            this.btnUpVB.UseVisualStyleBackColor = true;
            this.btnUpVB.Click += new System.EventHandler(this.btnUpVB_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(10, 9);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(116, 13);
            this.label16.TabIndex = 2;
            this.label16.Text = "Lưu văn bản liên thông";
            // 
            // txtFilePath
            // 
            this.txtFilePath.Location = new System.Drawing.Point(8, 34);
            this.txtFilePath.Name = "txtFilePath";
            this.txtFilePath.Size = new System.Drawing.Size(239, 20);
            this.txtFilePath.TabIndex = 1;
            // 
            // btnBrowser
            // 
            this.btnBrowser.Location = new System.Drawing.Point(255, 32);
            this.btnBrowser.Name = "btnBrowser";
            this.btnBrowser.Size = new System.Drawing.Size(75, 23);
            this.btnBrowser.TabIndex = 0;
            this.btnBrowser.Text = "Chọn tệp";
            this.btnBrowser.UseVisualStyleBackColor = true;
            this.btnBrowser.Click += new System.EventHandler(this.btnBrowser_Click);
            // 
            // btnSaveConfig
            // 
            this.btnSaveConfig.Location = new System.Drawing.Point(151, 315);
            this.btnSaveConfig.Name = "btnSaveConfig";
            this.btnSaveConfig.Size = new System.Drawing.Size(119, 23);
            this.btnSaveConfig.TabIndex = 19;
            this.btnSaveConfig.Text = "Lưu thay đổi cấu hình";
            this.btnSaveConfig.UseVisualStyleBackColor = true;
            this.btnSaveConfig.Click += new System.EventHandler(this.btnSaveConfig_Click);
            // 
            // notifyTray
            // 
            this.notifyTray.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyTray.Icon")));
            this.notifyTray.Text = "Thông báo";
            this.notifyTray.Visible = true;
            this.notifyTray.BalloonTipClicked += new System.EventHandler(this.notifyTray_BalloonTipClicked);
            this.notifyTray.Click += new System.EventHandler(this.notifyTray_Click);
            this.notifyTray.MouseClick += new System.Windows.Forms.MouseEventHandler(this.notifyTray_MouseClick);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(276, 315);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 16;
            this.btnExit.Text = "Thoát";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnLogs
            // 
            this.btnLogs.Location = new System.Drawing.Point(70, 315);
            this.btnLogs.Name = "btnLogs";
            this.btnLogs.Size = new System.Drawing.Size(75, 23);
            this.btnLogs.TabIndex = 20;
            this.btnLogs.Text = "Xem Logs";
            this.btnLogs.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(9, 46);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(74, 13);
            this.label17.TabIndex = 27;
            this.label17.Text = "Tên ứng dụng";
            // 
            // txtApplicationName
            // 
            this.txtApplicationName.Location = new System.Drawing.Point(110, 43);
            this.txtApplicationName.Name = "txtApplicationName";
            this.txtApplicationName.Size = new System.Drawing.Size(98, 20);
            this.txtApplicationName.TabIndex = 28;
            this.txtApplicationName.Text = "ubbacgiang";
            // 
            // hcm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(355, 363);
            this.Controls.Add(this.btnLogs);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnSaveConfig);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.statusLienThong);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "hcm";
            this.Text = "Liên thông văn bản Hồ Chí Minh";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.hcm_FormClosing);
            this.Load += new System.EventHandler(this.hcm_Load);
            this.Resize += new System.EventHandler(this.hcm_Resize);
            this.groupDomino.ResumeLayout(false);
            this.groupDomino.PerformLayout();
            this.groupLienThong.ResumeLayout(false);
            this.groupLienThong.PerformLayout();
            this.statusLienThong.ResumeLayout(false);
            this.statusLienThong.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtMatKhau;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtTaiKhoan;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.Button btnRecieve;
        private System.Windows.Forms.TextBox txtApiUrl;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupDomino;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtTDUrl;
        private System.Windows.Forms.Button btnTDLogin;
        private System.Windows.Forms.Button btnTDDisConnect;
        private System.Windows.Forms.TextBox txtTDPw;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtTDUser;
        private System.Windows.Forms.GroupBox groupLienThong;
        private System.Windows.Forms.StatusStrip statusLienThong;
        private System.Windows.Forms.ToolStripStatusLabel toolStatus;
        private System.Windows.Forms.ToolStripProgressBar toolStatusProcess;
        private System.ComponentModel.BackgroundWorker bgTransferVB;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btnSaveConfig;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtFtpUrl;
        private System.Windows.Forms.TextBox txtFtpPassword;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtFtpUsername;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtVBGui;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtVBNhan;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtTime;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtFolder;
        private System.Windows.Forms.Button btnCapNhatDonVi;
        private System.Windows.Forms.NotifyIcon notifyTray;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtServices;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btnBrowser;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtFilePath;
        private System.Windows.Forms.Button btnUpVB;
        private System.Windows.Forms.Button btnLogs;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtApplicationName;
    }
}