﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace DemoBTT
{
    public class LotusConnection
    {
        public static string CurrentNoteVersion = string.Empty;
        //private string serveradd = "http://192.168.2.206:8083";
        private string _Host = "127.0.0.1";
        private int _Port = 80;
        private string _User = String.Empty;
        private string _Pass = String.Empty;
        private bool login = false;
        public static CookieContainer cookies = null;
        public static string userMailFile = string.Empty;
        private static string urlPattern = "[host]/names.nsf?login&username=[user]&password=[pass]&RedirectTo=[host]/names.nsf";
        public string Host
        {
            get { return this._Host; }
            set { this._Host = value; }
        }
        public int Port
        {
            get { return this._Port; }
            set { this._Port = value; }
        }
        public string UserName
        {
            get { return this._User; }
            set { this._User = value; }
        }
        public string Password
        {
            get { return this._Pass; }
            set { this._Pass = value; }
        }
        public bool Status
        {
            get { return this.login; }
        }
        public LotusConnection()
        {

        }
        public LotusConnection(string host, int port, string user, string pass)
        {
            this._Host = host;
            this._Port = port;
            this._User = user;
            this._Pass = pass;
        }
        public bool Init()
        {
            string url = urlPattern.Replace("[host]", this._Host);
            url = url.Replace("[user]", this._User);
            url = url.Replace("[pass]", this._Pass);
            HttpWebRequest req = null;
            HttpWebResponse response = null;
            try
            {
                cookies = new CookieContainer();
                Uri uri = new Uri(url);
                req = (HttpWebRequest)WebRequest.Create(uri);
                req.CookieContainer = cookies;
                req.KeepAlive = true;
                req.Timeout = 60000;
                req.ProtocolVersion = HttpVersion.Version10;
                req.Connection = "keepalive";
                response = (HttpWebResponse)req.GetResponse();
                CookieCollection cookCol = cookies.GetCookies(uri);
                if (cookCol.Count > 0)
                {
                    this.login = true;
                }
                else this.login = false;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (response != null) response.Close();
                req = null;
                response = null;
            }
            return this.login;
        }
        public bool CheckConnectServer(string serverUrl)
        {
            HttpWebRequest req = null;
            try
            {
                string urlPattern = serverUrl + "/names.nsf";
                string url = urlPattern;
                Uri uri = new Uri(url);
                req = (HttpWebRequest)WebRequest.Create(uri);
                req.Timeout = 30000;
                HttpWebResponse response = (HttpWebResponse)req.GetResponse();
                response.Close();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
            }
        }
        public static void Close(string serverUrl)
        {
            HttpWebRequest req;
            HttpWebResponse response;
            try
            {
                string serveradd =serverUrl ;
                string url = "[host]/names.nsf?logout";
                url = url.Replace("[host]", serveradd);
                //url = url.Replace("[port]", LotusConfigurations.Port.ToString());
                Uri uri = new Uri(url);
                req = (HttpWebRequest)WebRequest.Create(uri);
                req.CookieContainer = cookies;
                response = (HttpWebResponse)req.GetResponse();
                response.Close();
            }
            catch (Exception ex)
            {

                return;
            }
            finally
            {
                req = null;
                response = null;
                cookies = null;
            }
        }
        public static void OpenURL(string url)
        {
            try
            {
                System.Diagnostics.Process.Start(url);
            }
            catch (Exception ex)
            {
                //if (Common.AppConfig.debug)
                //Common.AppConfig.WriteLog(ex.Message);
            }
        }
        private string ExtractNumber(string toExtract)
        {
            string resultString = System.Text.RegularExpressions.Regex.Match(toExtract, @"\d+").Value;
            return resultString;
        }
    }
}
