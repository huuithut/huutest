﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Bkav.edXml.Api.Util;
using Bkav.edXml.Api.Entity;
using Bkav.edXml.Api;
using System.Net;
using System.IO;

namespace DemoBTT
{
    public partial class frmMain : Form
    {
        //EdocBTT.CentreServices eDoc = new EdocBTT.CentreServices();
        string serviceUrl = "http://www.test.e-doc.vn/edxmlservices.asmx";
        string ftpserver = "ftp://192.168.2.7/Khach_Hang(Anonymous)/ctthanh-dungcoxoa/";
        string ftpuser = "tandan\\ctthanh";
        string ftppass = "thanhcao";
        DataContext eDocContext;
        TDGetEmailList.Service tdService = new TDGetEmailList.Service();
        GetDocumentRecieve.Service tdSaveDocService = new GetDocumentRecieve.Service();
        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            //eDocContext = new DataContext(serviceUrl);
            //bool login = eDocContext.Login("tandan", "123456");
            //var dsloaivanban = eDocContext.GetAllMessageType();
            //var loaivanban = dsloaivanban.Find(delegate(MessageType p) { return p.Description.Trim() == "Điện báo".ToLower(); });
            //if (loaivanban != null)
            //{
            //    MessageBox.Show(loaivanban.Description);
            //}
            //else
            //{
            //    MessageBox.Show("NUll");
            //}
            //DataSet1 ds = new DataSet1();
            //DataSet1TableAdapters.testTableAdapter a = new DataSet1TableAdapters.testTableAdapter();
            //try
            //{
            //    a.Insert("a", "b");
            //    ds.test.AddtestRow("1", "Tiêu đề");
            //    ds.test.AcceptChanges();
            //    ds.AcceptChanges();
            //}

            //catch (Exception ex)
            //{
            //    MessageBox.Show("An error occurred during the update process");
            //    // Add code to handle error here.
            //}
            //finally
            //{
            //}
            //FileStream readStream;
            //string msg = null;
            //try
            //{
            //    readStream = new FileStream("D:\\HuongDanLienThong.doc", FileMode.Open);
            //    BinaryReader readBinary = new BinaryReader(readStream);
            //    msg = readBinary.ReadString();
            //    MessageBox.Show(msg);
            //    readStream.Close();
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.ToString());
            //}
            //MessageBox.Show(FileToByteArray("D:\\HuongDanLienThong.doc").Length.ToString());
            //MessageBox.Show(Convert.ToBase64String(FileToByteArray("D:\\HuongDanLienThong.doc")));
        }
        public byte[] FileToByteArray(string _FileName)
        {
            byte[] buffer = null;
            try
            {
                // Open file for reading 
                System.IO.FileStream _FileStream = new System.IO.FileStream(_FileName, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                // attach filestream to binary reader 
                System.IO.BinaryReader _BinaryReader = new System.IO.BinaryReader(_FileStream);
                // get total byte length of the file 
                long _TotalBytes = new System.IO.FileInfo(_FileName).Length;
                // read entire file into buffer 
                buffer = _BinaryReader.ReadBytes((Int32)_TotalBytes);
                // close file reader 
                _FileStream.Close();
                _FileStream.Dispose();
                _BinaryReader.Close();
            }
            catch (Exception _Exception)
            {
                // Error 
                Console.WriteLine("Exception caught in process: {0}", _Exception.ToString());
            }
            return buffer;
        }
        public string ConvertAnyFileToBase64String(string fileinputpath)
        {
            System.IO.FileStream inFile;
            byte[] binaryData;
            string base64String;
            try
            {
                inFile = new System.IO.FileStream(fileinputpath, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                binaryData = new Byte[inFile.Length];
                long bytesRead = inFile.Read(binaryData, 0, (int)inFile.Length);
                inFile.Close();
            }
            catch (Exception exp)
            {
                return "";
            }
            try
            {
                base64String = System.Convert.ToBase64String(binaryData, 0, binaryData.Length);
            }
            catch (ArgumentNullException)
            {
                return "";
            }
            return base64String;
            //File.WriteAllBytes("123.pdf", Convert.FromBase64String(base64String));
        }


        private void btnSend_Click(object sender, EventArgs e)
        {
            GetDocSendTo();
        }

        private void btnRecieve_Click(object sender, EventArgs e)
        {
            GetAllDocRecieve();
        }

        private void GetDocSendTo()
        {
            eDocContext = new DataContext(serviceUrl);
            bool login = eDocContext.Login("tandan", "123456");
            if (login)
            {
                TDGetEmailList.GuiSMS[] dsVB = tdService.LayDanhSachEmailVBdiCanGuiDi("hanam/vbdi.nsf");
                if (dsVB != null)
                {
                    foreach (var vbdi in dsVB)
                    {
                        if (vbdi != null)
                        {
                            Bkav.edXml.Api.Entity.Message msgVanBan = new Bkav.edXml.Api.Entity.Message();
                            msgVanBan.Id = RandomNumber(20, 10000);
                            msgVanBan.CodeNumber = "Số\\"; // số văn bản
                            msgVanBan.CodeNotation = "Ký Hiệu"; // ký hiệu văn bản
                            msgVanBan.Place = GetString(Convert.FromBase64String(vbdi.STRNOIGUI)); // cơ quan ban hành
                            msgVanBan.Date = DateTime.Now; //ngày ban hành;
                            //MessageType msgType = eDocContext.GetAllMessageType()[0];
                            //msgType.Code = "msgType";
                            //msgType.Description = "msgTypeDesc";
                            //msgVanBan.MessageType = msgType;

                            var dsloaivanban = eDocContext.GetAllMessageType();
                            var loaivanban = dsloaivanban.Find(delegate(MessageType p) { return p.Description.Trim() == GetString(Convert.FromBase64String(vbdi.STRLOAIVANBAN)); });
                            if (loaivanban != null)
                            {
                                msgVanBan.MessageType = loaivanban;
                            }
                            else
                            {
                                MessageType msgType = new MessageType();
                                msgType.Code = "0001";
                                msgType.Description = GetString(Convert.FromBase64String(vbdi.STRLOAIVANBAN));
                                msgVanBan.MessageType = msgType;
                            }
                            msgVanBan.Subject = GetString(Convert.FromBase64String(vbdi.Subject)); //
                            msgVanBan.Content = GetString(Convert.FromBase64String(vbdi.TrichYeu)); // trích yếu
                            msgVanBan.Secret = 1; // độ mật
                            msgVanBan.Priority = 1; // độ khẩn
                            msgVanBan.SphereOfPromulgation = "Phạm vi ban hành";
                            msgVanBan.ContactInfo = "Thông tin liên hệ";
                            msgVanBan.TyperNotation = "Thông tin người đánh máy";
                            msgVanBan.PromulgationAmount = 1;
                            msgVanBan.PageAmount = 1;
                            msgVanBan.AuthorCompetence = "Người ký: Chức vụ";
                            msgVanBan.AuthorFunction = "Quyền người ký";
                            msgVanBan.AuthorFullName = GetString(Convert.FromBase64String(vbdi.STRNGUOIKY)); // người ký
                            msgVanBan.ToPlaces = new List<Place> { };
                            msgVanBan.From = eDocContext.GetCustomer()[0];
                            List<Customer> listSendTo = new List<Customer>();
                            foreach (var cus in eDocContext.GetCustomer())
                            {
                                if (cus != null)
                                {
                                    if (cus.Username.ToLower() == GetString(Convert.FromBase64String(vbdi.SendTo)).ToLower())
                                    {
                                        listSendTo.Add(cus);
                                    }
                                }
                            }
                            msgVanBan.To = listSendTo;
                            string _getFileDownloadUrl = "";
                            string localFileName = "";
                            if (!string.IsNullOrEmpty(vbdi.DinhKemFile))
                            {
                                _getFileDownloadUrl = vbdi.DinhKemFile;
                                foreach (string _filedinhkem in _getFileDownloadUrl.Split(';'))
                                {
                                    if (!string.IsNullOrEmpty(_filedinhkem))
                                    {
                                        string getFileName = _filedinhkem.Substring(_filedinhkem.LastIndexOf("/") + 1);
                                        localFileName += Application.StartupPath + "\\" + getFileName + ";";
                                        Uri _downloadUrl = new Uri(_filedinhkem);
                                        DownloadAttachmentFromLotus(Application.StartupPath + "\\" + getFileName, _downloadUrl);
                                    }
                                }
                            }
                            List<Attachment> atts = new List<Attachment>();
                            if (!string.IsNullOrEmpty(localFileName))
                            {
                                foreach (string fileto in localFileName.Split(';'))
                                {
                                    if (!string.IsNullOrEmpty(fileto))
                                    {
                                        string filecontent = File.ReadAllText(fileto);
                                        Attachment att = new Attachment();
                                        att.MessageId = msgVanBan.Id;
                                        att.Name = Path.GetFileName(fileto);
                                        att.Value = StringToStringBase64(filecontent);
                                        att.Encoding = "UTF-8";
                                        att.ContentType = Path.GetExtension(fileto);
                                        atts.Add(att);
                                    }
                                }
                            }
                            msgVanBan.Attachments = atts;
                            try
                            {
                                ServerResponse sendResult = eDocContext.SendMessage(msgVanBan, null);
                                if (sendResult.Success)
                                {
                                    MessageBox.Show("Gui thanh cong");
                                }
                                else
                                {
                                    MessageBox.Show(sendResult.Report);
                                }
                            }
                            catch (Bkav.edXml.Api.EdXmlException ex)
                            {
                                MessageBox.Show(ex.Message);
                            }
                        }
                    }
                }
                #region backup
                //Bkav.edXml.Api.Entity.Message msgVanBan = new Bkav.edXml.Api.Entity.Message();
                ////msgVanBan.Id = 1;
                //msgVanBan.CodeNumber = "CodeNumber";
                //msgVanBan.CodeNotation = "CodeNotation";
                //msgVanBan.Place = "Place";
                //msgVanBan.Date = DateTime.Now;
                //MessageType msgType = eDocContext.GetAllMessageType()[0];
                //msgType.Code = "msgType";
                //msgType.Description = "msgTypeDesc";
                //msgVanBan.MessageType = msgType;
                //msgVanBan.Subject = "Subject = Test thu 1 cai";
                //msgVanBan.Content = "Content";
                //msgVanBan.Secret = 1;
                //msgVanBan.Priority = 1;
                //msgVanBan.SphereOfPromulgation = "SphereOfPromulgation";
                //msgVanBan.ContactInfo = "ContactInfo";
                //msgVanBan.TyperNotation = "TyperNotation";
                //msgVanBan.PromulgationAmount = 1;
                //msgVanBan.PageAmount = 1;
                //msgVanBan.AuthorCompetence = "Authorcompetence";
                //msgVanBan.AuthorFunction = "AuthorFunction";
                //msgVanBan.AuthorFullName = "AuthorFullName";
                //msgVanBan.ToPlaces = new List<Place> { };
                //msgVanBan.From = eDocContext.GetCustomer()[0];
                //msgVanBan.To = eDocContext.GetCustomer();
                //try
                //{
                //    ServerResponse sendResult = eDocContext.SendMessage(msgVanBan, null);
                //    if (sendResult.Success)
                //    {
                //        MessageBox.Show("Gui thanh cong");
                //    }
                //    else
                //    {
                //        MessageBox.Show(sendResult.Report);
                //    }
                //}
                //catch (Bkav.edXml.Api.EdXmlException ex)
                //{
                //    MessageBox.Show(ex.Message);
                //}
                #endregion
            }
            else
            {
                MessageBox.Show("that bai");
            }
        }

        private void GetAllDocRecieve()
        {
            eDocContext = new DataContext(serviceUrl);
            bool login = eDocContext.Login("tandan1", "123456");
            if (login)
            {
                var listAllDoc = eDocContext.GetNotification();
                if (listAllDoc != null)
                {
                    foreach (var doc in listAllDoc)
                    {
                        if (doc != null)
                        {
                            var docInfo = eDocContext.GetMessage(doc.MessageId);
                            List<Attachment> atts = docInfo.Attachments;
                            string attnames = "";
                            if (atts != null)
                            {
                                foreach (var att in atts)
                                {
                                    if (att != null)
                                    {
                                        File.WriteAllText(att.Name, GetString(Convert.FromBase64String(att.Value)));
                                        UploadFileToFtp(att.Name, ftpserver, ftpuser, ftppass);
                                        attnames += att.Name + ";";
                                    }
                                }
                            }
                            var save = tdSaveDocService.LuuEmailMoiLenServer("hanam/vbden.nsf", StringToStringBase64(docInfo.Id.ToString()), StringToStringBase64(docInfo.Subject), StringToStringBase64("tandan@tandan.com.vn"), StringToStringBase64(docInfo.Content), StringToStringBase64("20/08/2010"), ftpserver, attnames, "");
                            foreach (string _att in attnames.Split(';'))
                            {
                                if (!string.IsNullOrEmpty(_att.Trim()))
                                {
                                    DeleteFileFromFtp(ftpserver, _att.Trim(), ftpuser, ftppass);
                                    DeleteFileFromTemp(_att.Trim());
                                }
                            }
                            eDocContext.ConfirmReceivedMessage(doc.MessageId);
                        }
                    }
                }
            }
            MessageBox.Show("Đã nhận về");
        }

        public static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }
        public static string GetString(byte[] bytes)
        {
            return System.Text.Encoding.UTF8.GetString(bytes);
        }
        public static string DownloadAttachmentFromLotus(string strFileSaveFileandPath, Uri _uri)
        {
            string downloadfile = "";
            HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(_uri);
            wr.CookieContainer = new CookieContainer();
            //wr.CookieContainer.SetCookies(_uri, controls.Document.Cookie);
            //wr.CookieContainer = Classes.LotusConnections.cookies;
            HttpWebResponse ws = (HttpWebResponse)wr.GetResponse();
            Stream str = ws.GetResponseStream();
            byte[] inBuf = new byte[100000];
            int bytesToRead = (int)inBuf.Length;
            int bytesRead = 0;
            while (bytesToRead > 0)
            {
                int n = str.Read(inBuf, bytesRead, bytesToRead);
                if (n == 0)
                    break;
                bytesRead += n;
                bytesToRead -= n;
            }
            FileStream fstr = new FileStream(strFileSaveFileandPath, FileMode.OpenOrCreate, FileAccess.Write);
            fstr.Write(inBuf, 0, bytesRead);
            str.Close();
            fstr.Close();
            return downloadfile;
        }
        public static string StringToStringBase64(string inputString)
        {
            string rs64 = "";
            var bytes = Encoding.UTF8.GetBytes(inputString);
            rs64 = Convert.ToBase64String(bytes);
            return rs64;
        }
        static int RandomNumber(int min, int max)
        {
            Random random = new Random(); return random.Next(1, 1000);
        }
        public static bool UploadFileToFtp(string fullfilepath, string ftpUrlToUpload, string ftpUserAcc, string ftpUserPass)
        {
            bool ketquauploadfile = true;
            //"ftp://demo.tandan.com.vn/Khach_Hang(Anonymous)/ctthanh-dungcoxoa/"
            if (!string.IsNullOrEmpty(fullfilepath))
            {
                try
                {
                    FileInfo _filetoupload = new FileInfo(fullfilepath);
                    FtpWebRequest _srToupload = (FtpWebRequest)WebRequest.Create(ftpUrlToUpload + _filetoupload.Name);
                    _srToupload.Method = WebRequestMethods.Ftp.UploadFile;
                    _srToupload.Credentials = new NetworkCredential(ftpUserAcc, ftpUserPass);
                    Stream _stream = _srToupload.GetRequestStream();
                    FileStream _file = File.OpenRead(fullfilepath);
                    int length = 1024;
                    byte[] buffer = new byte[length];
                    int byteRead = 0;
                    do
                    {
                        byteRead = _file.Read(buffer, 0, length);
                        _stream.Write(buffer, 0, byteRead);
                    }
                    while (byteRead != 0);
                    _file.Close();
                    _stream.Close();
                }
                catch (Exception ex)
                {
                    ketquauploadfile = false;
                }
            }
            else
            {
                return ketquauploadfile = false;
            }
            return ketquauploadfile;
        }
        public static void DeleteFileFromFtp(string ftpPath, string ftpFileName, string ftpUserAcc, string ftpUserPass)
        {
            string fileName = ftpFileName;
            try
            {
                if (!RemoteFileExists(ftpPath + ftpFileName))
                {
                    FtpWebRequest requestFileDelete = (FtpWebRequest)WebRequest.Create(ftpPath + fileName);
                    requestFileDelete.Credentials = new NetworkCredential(ftpUserAcc, ftpUserPass);
                    requestFileDelete.Method = WebRequestMethods.Ftp.DeleteFile;
                    FtpWebResponse responseFileDelete = (FtpWebResponse)requestFileDelete.GetResponse();
                }
            }
            catch (Exception)
            {
                //MessageBox.Show(ftpFileName);
            }
        }
        public static bool RemoteFileExists(string url)
        {
            try
            {
                //Creating the HttpWebRequest
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                //Setting the Request method HEAD, you can also use GET too.
                request.Timeout = 10;
                request.Method = "HEAD";
                //Getting the Web Response.
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                //Returns TRUE if the Status code == 200
                return (response.StatusCode == HttpStatusCode.OK);
            }
            catch
            {
                //Any exception will returns false.
                return false;
            }
        }
        public static bool DeleteFileFromTemp(string fullFilePath)
        {
            try
            {
                if (File.Exists(fullFilePath))
                {
                    File.Delete(fullFilePath);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (IOException)
            {
                return false;
            }
        }
    }
}
//                 pw.println("<table cellpadding=0 cellspacing=0 width=100% border=0 style=display:none>");
//                 pw.println("<tr><td align=center height="+(height+10)+"><font style='font-size:16pt'><b>PHI\u1ebeU X\u1eec L\u00dd V\u0102N B\u1ea2N \u0110\u1ebeN</font></b></td></tr>");
//                 pw.println("<tr><td height=5 align=center>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S? ??N: "+soden+"</td></tr>");
//                // pw.println("<tr><td align=center  valign=center height="+(height+10)+"><b>V?N PHÒNG "+tentochuc.toUpperCase()+" nh?n: </b></td></tr>");
//                 pw.println("<tr><td align=center  valign=center height="+(height+10)+"><b>"+tentochuc.toUpperCase()+" nh?n: </b></td></tr>");
//                 pw.println("<tr><td height="+(height+10)+">Công v?n s?: "+ sokyhieu +"  "+ thoigian +"</td></tr>");
//                 pw.println("<tr><td height="+(height+10)+">C? quan g?i: <b>"+coquangui +"</b></td></tr>"); 	  	 
//                 pw.println("<tr><td height=10>&nbsp;</td></tr>"); 	  	 
//                 pw.println("<tr><td align=center height="+(height+10)+"><font style='font-size:16pt'><b>Ý KI?N ?? XU?T C?A V?N PHÒNG</font></b></td></tr>");	 
//                 pw.println("<tr><td>"+xlsobocuavp+"&nbsp;</td></tr>");	 
//                 pw.println("</table></td>");



//                 pw.println("<td valign=top rowspan=2 width=30% style='border-left:1px solid;display:none;'>");
//                 pw.println("<table cellpadding=0 cellspacing=0 width=100% border=0 valign=top height=100%>");
//                 pw.println("<tr><td align=center height="+(height+10)+"><font style='font-size:16pt' ><b>Ý KI?N LÃNH ??O</b></font></td></tr>");

//                 pw.println("<tr><td height=100%>"+chidaocuald+"&nbsp;</td></tr>");	 
//                 pw.println("</table>");
//                 pw.println("</td>");
//                 pw.println("</tr>");



//                 pw.println("<tr>");					 
//                 pw.println("<td>");
//                 pw.println("<table width=100% border=0 cellpadding=0 cellspacing=0 style=display:none>"); 
//                 //===========beign 
//                 pw.println("<tr><td colspan=2 width=15% valign=top align=left nowrap height="+height+"><b><i>Kính g?i:</i></b></td>");	 
//                // 	 pw.println("<tr><td rowspan=2 width=15% valign=top align=right nowrap><b><i>Kính g?i:</i></b></td>");

//                 pw.println("<tr><td rowspan=2 width=20>&nbsp;</td>");	 
//                 pw.println("<td>");
//                 String nhandebiet=doc.getItemValueString("VBdenNhapmoi_nhandebiet");
//                 if (nhandebiet==null) nhandebiet="";
//                 String[] array=split(nhandebiet,";");
//                 if (array.length>0){
//                    //System.out.println(array.length);	 
//                    pw.println("<table width=100% height=100% border=0 cellpadding=0 cellspacing=0 style=display:none>");
//                    pw.println("<tr>");
//                    pw.println("<td width=70% height="+height+">");
//                    pw.println("<table width=100% height=100% border=0 cellpadding=0 cellspacing=0 >");
//                    for (int j=0;j<array.length;j++)
//                        pw.println("<tr><td height="+height+" style='border-right:1px solid  '> - ?/c "+array[j]+", "+getTTChucVu(array[j])+"</td></tr>");
//                    pw.println("<tr><td width=1>&nbsp;</td></tr>");	 			
//                    pw.println("</table");
//                    pw.println("</td>");
//                    pw.println("<td width=1>&nbsp;</td>");	 			
//                    pw.println("<td align=left valign=top >?? bi?t và cho ý ki?n ch? ??o</td>");
//                    pw.println("</tr>");
//                    pw.println("</table>");
//                  }	

//                    pw.println("</td>");
//                    pw.println("</tr>");
//                     //}
//                 // table 2

//                    // 	 String phoihopxuly=doc.getItemValueString("VBdenNhapmoi_nhandebiet");

//                    pw.println("<tr>");
//                    pw.println("<td>");

//                    nhandebiet=doc.getFirstItem("VBdenNhapmoi_donvixuly").getText();
//                    if (nhandebiet==null) nhandebiet="";
//                    if (!nhandebiet.equals("")){						 
//                        pw.println("<table width=100% height=100% border=0 cellpadding=0 cellspacing=0>");
//                        pw.println("<tr>");
//                        pw.println("<td width=70% height="+height+">");
//                        pw.println("<table width=100% height=100% border=0 cellpadding=0 cellspacing=0>");
//                        pw.println("<tr><td height="+height+" style='border-right:1px solid  '> - "+nhandebiet+"</td></tr>");			 		
//                        pw.println("<tr><td height="+height+">&nbsp;</td></tr>");
//                        pw.println("</table");
//                        pw.println("</td>");
//                        pw.println("<td width=1>&nbsp;</td>");	 			
//                        pw.println("<td align=left valign=top >??n v? x? lý</td>");
//                        pw.println("</tr>");
//                        pw.println("</table>");
//                    }

//                    nhandebiet=doc.getFirstItem("VBdenNhapmoi_donviphoihop").getText();

//                    if (nhandebiet==null) nhandebiet="";
//                    String[] arraydv=split(nhandebiet,";");
//                    if (arraydv.length>0){						 
//                        pw.println("<table width=100% height=100% border=0 cellpadding=0 cellspacing=0 style=display:none>");
//                        pw.println("<tr>");
//                        pw.println("<td width=70% height="+height+">");
//                        pw.println("<table width=100% height=100% border=0 cellpadding=0 cellspacing=0>");



//                        for ( int j=0;j<arraydv.length;j++)
//                            pw.println("<tr><td height="+height+" style='border-right:1px solid  '> - "+arraydv[j]+"</td></tr>");			 		
//                        pw.println("</table");
//                        pw.println("</td>");
//                        pw.println("<td width=1>&nbsp;</td>");	 			
//                        pw.println("<td align=left valign=top >?? bi?t, ph?i h?p x? lý</td>");
//                        pw.println("</tr>");
//                        pw.println("</table>");
//                    }
//                     pw.println("</td>");
//                     pw.println("</tr>");	
//                     // end table 2 
//                     //============end
//                     pw.println("</table></td>");	 
//                     pw.println("</tr>");
///*String nguoixuly = doc.getItemValueString("VBdenNhapmoi_chuyenchuyenvienXLC");
//if (nguoixuly==null) nguoixuly="";
//if(nguoixuly.equals(""))
//    nguoixuly = doc.getItemValueString("VBdenNhapmoi_cualanhdao");		
//*/
//                     pw.println("</table></td></tr>");
//                     pw.println("</table> ");