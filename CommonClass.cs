﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Microsoft.Win32;
using System.Windows.Forms;
using System.Reflection;
using System.IO;
using System.Text.RegularExpressions;
using System.Net;
using System.Configuration;
using System.Data;

namespace DemoBTT
{
    class CommonClass
    {
        public static string location = Application.StartupPath;
        private const string RUN_LOCATION = @"Software\Microsoft\Windows\CurrentVersion\Run";
        private string keyname = "TDHSCVOffline";
        private string assemblyLocation = Assembly.GetExecutingAssembly().Location;
        //public static string DBName = ConfigurationSettings.AppSettings["SmsDatabaseName"].ToString();
        //public static string ServerUrl = ConfigurationSettings.AppSettings["ServerUrl"].ToString();
        //public static string TuDienUrl = ConfigurationSettings.AppSettings["TuDienUrl"].ToString();
        public static string FtpServer = ConfigurationSettings.AppSettings["FtpServer"].ToString();
        public static string FtpUserAccount = ConfigurationSettings.AppSettings["FtpUsername"].ToString();
        public static string FtpUserPassword = ConfigurationSettings.AppSettings["FtpPassword"].ToString();
        public static string HcmServerUrl = ConfigurationSettings.AppSettings["HcmServerUrl"].ToString();
        public static string AccessKey = ConfigurationSettings.AppSettings["AccessKey"].ToString();
        public static string SecretKey = ConfigurationSettings.AppSettings["SecretKey"].ToString();
        public static string TDServerUrl = ConfigurationSettings.AppSettings["TDServerUrl"].ToString();
        public static string TDUsername = ConfigurationSettings.AppSettings["TDUsername"].ToString();
        public static string TDPassword = ConfigurationSettings.AppSettings["TDPassword"].ToString();
        public static string TDVbdi = ConfigurationSettings.AppSettings["TDVbdi"].ToString();
        public static string TDVbden = ConfigurationSettings.AppSettings["TDVbden"].ToString();
        public static string TDFolder = ConfigurationSettings.AppSettings["TDFolder"].ToString();
        public static string TDApplicationName = ConfigurationSettings.AppSettings["ApplicationName"].ToString();
        //public static string GetmailtoDatabase = ConfigurationSettings.AppSettings["SaveMailDatabase"].ToString();
        //public static string AutoLogin = ConfigurationSettings.AppSettings["AutoLogin"].ToString();
        //public static string AutoToStartMail = ConfigurationSettings.AppSettings["AutoStartMail"].ToString();
        //public static string LayThu = ConfigurationSettings.AppSettings["LayThu"].ToString();
        //public static string GuiThu = ConfigurationSettings.AppSettings["GuiThu"].ToString();
        //public static string AutoStartGateWay = ConfigurationSettings.AppSettings["AutoStartGateWay"].ToString();
        //public static string CurrentEmailFilePathSetting = Path.GetDirectoryName(Application.ExecutablePath) + "\\datasettings\\emailsettings.xml";
        //public static string CurrentEmailFileLogPath = Path.GetDirectoryName(Application.ExecutablePath) + "\\datasettings\\downloadlog.xml";
        //public static string GetUserSettingFilePath = Path.GetDirectoryName(Application.ExecutablePath) + "\\datasettings\\userinfo.xml";
        //public static string TempAttDownloadTo = Path.GetDirectoryName(Application.ExecutablePath) + "\\temp\\";
        private static readonly string[] VietnameseSigns = new string[]
        {
            "aAeEoOuUiIdDyY",
            "áàạảãâấầậẩẫăắằặẳẵ",
            "ÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴ",
            "éèẹẻẽêếềệểễ",
            "ÉÈẸẺẼÊẾỀỆỂỄ",
            "óòọỏõôốồộổỗơớờợởỡ",
            "ÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠ",
            "úùụủũưứừựửữ",
            "ÚÙỤỦŨƯỨỪỰỬỮ",
            "íìịỉĩ",
            "ÍÌỊỈĨ",
            "đ",
            "Đ",
            "ýỳỵỷỹ",
            "ÝỲỴỶỸ"
        };
        public static string RemoveSign4VietnameseString(string str)
        {
            //Tiến hành thay thế , lọc bỏ dấu cho chuỗi
            for (int i = 1; i < VietnameseSigns.Length; i++)
            {
                for (int j = 0; j < VietnameseSigns[i].Length; j++)
                    str = str.Replace(VietnameseSigns[i][j], VietnameseSigns[0][i - 1]);
            }
            return str;
        }
        public static string CreateXmlFile(string filepath)
        {
            string ketqua = "Đã lưu cấu hình.";
            try
            {
                XmlDocument doc = new XmlDocument();
                XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
                doc.AppendChild(docNode);

                XmlNode productsNode = doc.CreateElement("settings");
                doc.AppendChild(productsNode);

                XmlNode productNode = doc.CreateElement("setting");
                XmlAttribute productAttribute = doc.CreateAttribute("id");
                productAttribute.Value = "01";
                productNode.Attributes.Append(productAttribute);
                productsNode.AppendChild(productNode);

                XmlNode nameNode = doc.CreateElement("comportName");
                nameNode.AppendChild(doc.CreateTextNode("ten cong"));
                productNode.AppendChild(nameNode);

                XmlNode priceNode = doc.CreateElement("TenDB");
                priceNode.AppendChild(doc.CreateTextNode("ten database"));
                productNode.AppendChild(priceNode);

                XmlNode timeNode = doc.CreateElement("Time");
                timeNode.AppendChild(doc.CreateTextNode("thoi gian gui"));
                productNode.AppendChild(timeNode);

                doc.Save(filepath);
            }
            catch (Exception ex)
            {
                ketqua = ex.Message;
            }
            return ketqua;
        }
        public static string CreateXmlFile(string filepath, string[] childNode, string[] nodeValue)
        {
            string ketqua = "Đã lưu cấu hình.";
            try
            {
                XmlDocument doc = new XmlDocument();
                XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
                doc.AppendChild(docNode);
                XmlNode productsNode = doc.CreateElement("settings");
                doc.AppendChild(productsNode);

                XmlNode productNode = doc.CreateElement("setting");
                productsNode.AppendChild(productNode);
                if (childNode.Count() > 0)
                {
                    int countNode = 0;
                    foreach (string _nodeName in childNode)
                    {
                        if (!string.IsNullOrEmpty(_nodeName))
                        {
                            XmlNode nameNode = doc.CreateElement(_nodeName);
                            nameNode.AppendChild(doc.CreateTextNode(nodeValue[countNode].ToString()));
                            productNode.AppendChild(nameNode);
                        }
                        countNode++;
                    }
                }
                doc.Save(filepath);
            }
            catch (Exception ex)
            {
                ketqua = ex.Message;
            }
            return ketqua;
        }
        public static void WriteSettings(string filepath, string[] childNode, string[] nodeValue)
        {
            DataSet dsSetting = new DataSet();
            dsSetting.ReadXml(filepath);
            DataTable dt = dsSetting.Tables[0];
            if (dt != null)
            {
                int count = 0;
                DataRow row = dt.Rows.Add();
                foreach (string _field in childNode)
                {
                    row[_field] = nodeValue[0].ToString();
                    count++;
                }
                dt.AcceptChanges();
                dsSetting.AcceptChanges();
                dsSetting.WriteXml(filepath);
            }
        }
        public static DataSet ReadSettings(string filepath)
        {
            DataSet dsSetting = new DataSet();
            dsSetting.ReadXml(filepath);
            //DataTable dt = dsSetting.Tables[0];
            return dsSetting;
        }
        public void setAutoStart(bool flag)
        {
            if (flag)
            {
                SetAutoStart(keyname, assemblyLocation);
            }
            else
            {
                if (IsAutoStartEnabled(keyname, assemblyLocation))
                    UnSetAutoStart(keyname);
            }
        }
        private static void SetAutoStart(string keyName, string assemblyLocation)
        {
            RegistryKey key = Registry.CurrentUser.CreateSubKey(RUN_LOCATION);
            key.SetValue(keyName, assemblyLocation);
        }
        public static bool IsAutoStartEnabled(string keyName, string assemblyLocation)
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey(RUN_LOCATION);
            if (key == null) return false;
            string value = (string)key.GetValue(keyName);
            if (value == null) return false;
            return (value == assemblyLocation);
        }
        private static void UnSetAutoStart(string keyName)
        {
            RegistryKey key = Registry.CurrentUser.CreateSubKey(RUN_LOCATION);
            key.DeleteValue(keyName);
        }
        public void SaveStreamToFile(string fileFullPath, Stream stream)
        {
            if (stream.Length == 0) return;

            // Create a FileStream object to write a stream to a file
            using (FileStream fileStream = System.IO.File.Create(fileFullPath, (int)stream.Length))
            {
                // Fill the bytes[] array with the stream data
                byte[] bytesInStream = new byte[stream.Length];
                stream.Read(bytesInStream, 0, (int)bytesInStream.Length);

                // Use FileStream object to write to the specified file
                fileStream.Write(bytesInStream, 0, bytesInStream.Length);
            }
        }
        public static string StringToStringBase64(string inputString)
        {
            string rs64 = "";
            if (!string.IsNullOrEmpty(inputString))
            {
                var bytes = Encoding.UTF8.GetBytes(inputString);
                rs64 = Convert.ToBase64String(bytes);
            }
            return rs64;
        }
        public static string BytesToStringBase64(string inputFileName)
        {
            string rs64 = "";
            byte[] _getByte = File.ReadAllBytes(inputFileName);
            //var bytes = Encoding.UTF8.GetBytes(inputFileName);
            rs64 = Convert.ToBase64String(_getByte);
            return rs64;
        }
        public static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }
        public static string GetString(byte[] bytes)
        {
            return System.Text.Encoding.UTF8.GetString(bytes);
        }
        public static bool ByteArrayToFile(string _FileName, byte[] _ByteArray)
        {
            try
            {
                // Open file for reading
                System.IO.FileStream _FileStream =
                   new System.IO.FileStream(_FileName, System.IO.FileMode.Create,
                                            System.IO.FileAccess.Write);
                // Writes a block of bytes to this stream using data from
                // a byte array.
                _FileStream.Write(_ByteArray, 0, _ByteArray.Length);
                // close file stream
                _FileStream.Close();
                return true;
            }
            catch (Exception _Exception)
            {
                // Error
                Console.WriteLine("Exception caught in process: {0}",
                                  _Exception.ToString());
            }
            // error occured, return false
            return false;
        }
        public static Stream GetStream(string str)
        {
            return new MemoryStream(GetBytes(str));
        }
        public static bool UploadFileToFtp(string fullfilepath, string ftpUrlToUpload, string ftpUserAcc, string ftpUserPass)
        {
            bool ketquauploadfile = true;
            //"ftp://demo.tandan.com.vn/Khach_Hang(Anonymous)/ctthanh-dungcoxoa/"
            if (!string.IsNullOrEmpty(fullfilepath))
            {
                try
                {
                    FileInfo _filetoupload = new FileInfo(fullfilepath);
                    FtpWebRequest _srToupload = (FtpWebRequest)WebRequest.Create(ftpUrlToUpload + _filetoupload.Name);
                    _srToupload.KeepAlive = true;
                    _srToupload.Method = WebRequestMethods.Ftp.UploadFile;
                    _srToupload.Credentials = new NetworkCredential(ftpUserAcc, ftpUserPass);
                    Stream _stream = _srToupload.GetRequestStream();
                    FileStream _file = File.OpenRead(fullfilepath);
                    int length = 20480;
                    byte[] buffer = new byte[length];
                    int byteRead = 0;
                    do
                    {
                        byteRead = _file.Read(buffer, 0, length);
                        _stream.Write(buffer, 0, byteRead);
                    }
                    while (byteRead != 0);
                    _file.Close();
                    _stream.Close();
                }
                catch (Exception ex)
                {
                    ketquauploadfile = false;
                }
            }
            else
            {
                return ketquauploadfile = false;
            }
            return ketquauploadfile;
        }
        public static void DeleteFileFromFtp(string ftpPath, string ftpFileName, string ftpUserAcc, string ftpUserPass)
        {
            string fileName = ftpFileName;
            try
            {
                if (!RemoteFileExists(ftpPath + ftpFileName))
                {
                    FtpWebRequest requestFileDelete = (FtpWebRequest)WebRequest.Create(ftpPath + fileName);
                    requestFileDelete.Credentials = new NetworkCredential(ftpUserAcc, ftpUserPass);
                    requestFileDelete.Method = WebRequestMethods.Ftp.DeleteFile;
                    FtpWebResponse responseFileDelete = (FtpWebResponse)requestFileDelete.GetResponse();
                }
            }
            catch (Exception)
            {
                //MessageBox.Show(ftpFileName);
            }
        }
        public static bool RemoteFileExists(string url)
        {
            try
            {
                //Creating the HttpWebRequest
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                //Setting the Request method HEAD, you can also use GET too.
                request.Timeout = 10;
                request.Method = "HEAD";
                //Getting the Web Response.
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                //Returns TRUE if the Status code == 200
                return (response.StatusCode == HttpStatusCode.OK);
            }
            catch
            {
                //Any exception will returns false.
                return false;
            }
        }
        public static bool DeleteFileFromTemp(string fullFilePath)
        {
            try
            {
                if (File.Exists(fullFilePath))
                {
                    File.Delete(fullFilePath);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (IOException)
            {
                return false;
            }
        }
        public static bool IsValidEmailAddress(string s)
        {
            var regex = new Regex(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
            return regex.IsMatch(s);
        }
        public static double ConvertMinutesToMilliseconds(double minutes)
        {
            return TimeSpan.FromMinutes(minutes).TotalMilliseconds;
        }
        public static double ConvertSecondToMilliseconds(double seconds)
        {
            return TimeSpan.FromSeconds(seconds).TotalMilliseconds;
        }
        public static string DownloadAttachmentFromLotus(string strFileSaveFileandPath, Uri _uri)
        {
            string downloadfile = "";
            HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(_uri);
            wr.CookieContainer = new CookieContainer();
            //wr.CookieContainer.SetCookies(_uri, controls.Document.Cookie);
            wr.CookieContainer = LotusConnection.cookies;
            HttpWebResponse ws = (HttpWebResponse)wr.GetResponse();
            Stream str = ws.GetResponseStream();
            byte[] inBuf = new byte[100000];
            int bytesToRead = (int)inBuf.Length;
            int bytesRead = 0;
            while (bytesToRead > 0)
            {
                int n = str.Read(inBuf, bytesRead, bytesToRead);
                if (n == 0)
                    break;
                bytesRead += n;
                bytesToRead -= n;
            }
            FileStream fstr = new FileStream(strFileSaveFileandPath, FileMode.OpenOrCreate, FileAccess.Write);
            fstr.Write(inBuf, 0, bytesRead);
            str.Close();
            fstr.Close();
            return downloadfile;
        }
        public static string DacTaVanBan(string docnguoinhan, string sotrang, string domat, string sott, string soban, string ngayky, string ngaybanhanh, string chucvu, string dokhan,
           string coquanbanhanh, string ghichu, string nguoisoanthao, string kyhieu, string sokyhieu, string nguoiky, string trichyeu, string loaivanban, string linhvuc, string madonvi)
        {
            string dacta = @"<TONGHOP>
                                <DOCNGUOINHAN><![CDATA[" + docnguoinhan + @"]]></DOCNGUOINHAN>
                                <SOTRANG><![CDATA[" + sotrang + @"]]></SOTRANG>
                                <DOMAT><![CDATA[" + domat + @"]]></DOMAT>
                                <SOTHUTU><![CDATA[" + sott + @"]]></SOTHUTU>
                                <SOBAN><![CDATA[" + soban + @"]]></SOBAN>
                                <NGAYKY><![CDATA[" + ngayky + @"]]></NGAYKY>
                                <NGAYBANHANH><![CDATA[" + ngaybanhanh + @"]]></NGAYBANHANH>
                                <CHUCVU><![CDATA[" + chucvu + @"]]></CHUCVU>
                                <DOKHAN><![CDATA[" + dokhan + @"]]></DOKHAN>
                                <COQUANBANHANH><![CDATA[" + coquanbanhanh + @"]]></COQUANBANHANH>
                                <GHICHU><![CDATA[" + ghichu + @"]]></GHICHU>
                                <NGUOISOANTHAO><![CDATA[" + nguoisoanthao + @"]]></NGUOISOANTHAO>
                                <KYHIEU><![CDATA[" + kyhieu + @"]]></KYHIEU>
                                <SOKYHIEU><![CDATA[" + sokyhieu + @"]]></SOKYHIEU>
                                <NGUOIKY><![CDATA[" + nguoiky + @"]]></NGUOIKY>
                                <TRICHYEU><![CDATA[" + trichyeu + @"]]></TRICHYEU>
                                <LOAIVANBAN><![CDATA[" + loaivanban + @"]]></LOAIVANBAN>
                                <LINHVUC><![CDATA[" + linhvuc + @"]]></LINHVUC>
                                <VBLT_LaVBLienThong><![CDATA[1]]></VBLT_LaVBLienThong>
                                <VBLT_MaDonVi><![CDATA[" + madonvi + @"]]></VBLT_MaDonVi>
                                <VBLT_SoKyHieuVB><![CDATA[" + sokyhieu + "/" + kyhieu + @"]]></VBLT_SoKyHieuVB>
                                <VBLT_NgayBanHanh><![CDATA[" + ngaybanhanh + @"]]></VBLT_NgayBanHanh>
                                <STRTDOFFICE>TDOffice</STRTDOFFICE>
                            </TONGHOP>";
            return dacta;
        }
        //public static bool CheckUserLoginHaveRolesTo(string TuDienNsf, string userloginname)
        //{
        //    bool userHasRole = true;
        //    GetLoginUserInfoService.Service _userInfo = new GetLoginUserInfoService.Service();
        //    GetLoginUserInfoService.GuiSMS[] infos = _userInfo.GetUserLoginInfo(TuDienNsf, userloginname);
        //    if (infos != null)
        //    {
        //        if (infos[0].rMessage == "True")
        //        {
        //            userHasRole = true;
        //        }
        //        else
        //        {
        //            userHasRole = false;
        //        }
        //    }
        //    _userInfo.Dispose();
        //    return userHasRole;
        //}
        public static void SaveAppSettingsKeyValueInAppConfig(string keyname, string keyvalue)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.AppSettings.Settings[keyname].Value = keyvalue;
            config.Save();
            ConfigurationManager.RefreshSection("appSettings");
        }
        public static void SaveApplicationSettings(string _value)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            ConfigurationSectionGroup applicationSectionGroup = config.GetSectionGroup("applicationSettings");
            ConfigurationSection applicationConfigSection = applicationSectionGroup.Sections["DemoBTT.Properties.Settings"];
            ClientSettingsSection clientSection = (ClientSettingsSection)applicationConfigSection;
            SettingElementCollection _collect = clientSection.Settings;
            if (_collect != null && _collect.Count > 0)
            {
                foreach (SettingElement applicationSetting in _collect)
                {
                    if (applicationSetting != null)
                    {
                        string _currentValue = applicationSetting.Value.ValueXml.InnerXml.ToLower();
                        if (_currentValue.Contains("SMSService.nsf".ToLower()))
                        {
                            string replaceValue = _value + _currentValue.Substring(_currentValue.IndexOf("SMSService.nsf".ToLower()) - 1);
                            applicationSetting.Value.ValueXml.InnerXml = replaceValue;
                            applicationConfigSection.SectionInformation.ForceSave = true;
                        }
                    }
                }
            }
            //SettingElement applicationSetting = clientSection.Settings.Get(settingname);
            //applicationSetting.Value.ValueXml.InnerXml = _value;
            //applicationConfigSection.SectionInformation.ForceSave = true;
            config.Save();
            ConfigurationManager.RefreshSection("applicationSettings");
        }
        public static bool IsConnectedToInternet
        {
            get
            {
                try
                {
                    HttpWebRequest hwebRequest = (HttpWebRequest)WebRequest.Create("http://google.com.vn");
                    hwebRequest.Timeout = 5000;
                    HttpWebResponse hWebResponse = (HttpWebResponse)hwebRequest.GetResponse();
                    if (hWebResponse.StatusCode == HttpStatusCode.OK)
                    {
                        return true;
                    }
                    else return false;
                }
                catch { return false; }
            }
        }
        public static byte[] ConvertToBytes(XmlDocument doc)
        {
            Encoding encoding = Encoding.UTF8;
            byte[] docAsBytes = encoding.GetBytes(doc.OuterXml);
            return docAsBytes;
        }
        #region Error Log
        public static void WriteErrorLog(string filename, string Message)
        {
            StreamWriter sw = null;
            try
            {
                //string sLogFormat = DateTime.Now.ToShortDateString().ToString() + " " + DateTime.Now.ToLongTimeString().ToString() + " ==> ";
                //string sPathName = @"Log_";
                //string sYear = DateTime.Now.Year.ToString();
                //string sMonth = DateTime.Now.Month.ToString();
                //string sDay = DateTime.Now.Day.ToString();
                //string sErrorTime = sDay + "-" + sMonth + "-" + sYear;
                //sw = new StreamWriter(sPathName + sErrorTime + ".txt", true);
                //sw.WriteLine(sLogFormat + Message);
                //sw.Flush();
                if (!File.Exists(filename))
                {
                    sw = new StreamWriter(filename, true);
                    sw.WriteLine(Message);
                    sw.Flush();
                    sw.Dispose();
                    sw.Close();
                }
                else
                {
                    WriteToStartOfFile(filename, Message);
                }
            }
            catch (Exception ex)
            {
                //ErrorLog(ex.ToString());
            }
            finally
            {
                if (sw != null)
                {
                    sw.Dispose();
                    sw.Close();
                }
            }

        }
        public static bool CheckEmailExists(string filepath, string mailIdToCheck)
        {
            int counter = 0;
            string line;
            // Read the file and display it line by line.
            System.IO.StreamReader file =
               new System.IO.StreamReader(filepath);
            while ((line = file.ReadLine()) != null)
            {
                if (mailIdToCheck.Trim().ToLower() == line.Trim().ToLower())
                {
                    return true;
                }
                counter++;
            }
            file.Close();
            file.Dispose();
            return false;
        }
        public static void WriteToStartOfFile(string file, string newValue)
        {
            using (var input = new StreamReader(file))
            using (var output = new StreamWriter(file + ".temp"))
            {
                output.WriteLine(newValue); // replace with your header :)
                var buf = new char[4096];
                int read = 0;
                do
                {
                    read = input.ReadBlock(buf, 0, buf.Length);
                    output.Write(buf, 0, read);
                } while (read > 0);

                output.Flush();
                output.Close();
                input.Close();
            }
            File.Replace(file + ".temp", file, file + ".backup");
        }
        #endregion
        public static byte[] GetBytesFromFile(string fullFilePath)
        {
            // this method is limited to 2^32 byte files (4.2 GB)

            FileStream fs = File.OpenRead(fullFilePath);
            try
            {
                byte[] bytes = new byte[fs.Length];
                fs.Read(bytes, 0, Convert.ToInt32(fs.Length));
                fs.Close();
                return bytes;
            }
            finally
            {
                fs.Close();
            }

        }
        public static string GetServiceLocation()
        {
            string serviceLocation = string.Empty;
            string defaultservices = DemoBTT.Properties.Settings.Default.DemoBTT_GetDocumentRecieve_Service.ToString().ToLower();
            string serviceFolder = defaultservices.Substring(0, defaultservices.IndexOf("SMSService.nsf".ToLower()) - 1);
            serviceLocation = serviceFolder;//.Substring(serviceFolder.LastIndexOf("/") + 1);
            //if (!string.IsNullOrEmpty(serviceLocation))
            //{
            //    serviceLocation = serviceLocation + "/SMSService.nsf";
            //}
            //else
            //{
            //    serviceLocation = "SMSService.nsf";
            //}
            return serviceLocation;
        }
        internal enum SoundType
        {
            NewClientEntered,
            NewMessageReceived,
            NewMessageWithPow,
            ClientExit
        }
        internal static void PlaySound(SoundType soundType)
        {
            System.Media.SoundPlayer player = new System.Media.SoundPlayer();
            switch (soundType)
            {
                case (SoundType.NewClientEntered):
                    player.SoundLocation = "Sounds/Knock.wav";
                    player.Play();
                    break;
                case (SoundType.ClientExit):
                    player.SoundLocation = "Sounds/Door.wav";
                    player.Play();
                    break;
                case (SoundType.NewMessageReceived):
                    player.SoundLocation = "Sounds/Message.wav";
                    player.Play();
                    break;
                case (SoundType.NewMessageWithPow):
                    player.SoundLocation = "Sounds/Pow.wav";
                    player.Play();
                    break;
            }
        }
        public static string CreateDescriptionFileForSendMail(string STRKYHIEU, string STRTRICHYEU, string STRNGAYKY, string STRNGUOIKY, string STRNOIGUI, string STRLOAIVANBAN)
        {
            string fileFormat = @"<EXPORTMAIL>" + Environment.NewLine +
                                  "<STRKYHIEU>{0}</STRKYHIEU>" + Environment.NewLine +
                                  "<STRTRICHYEU>{1}</STRTRICHYEU>" + Environment.NewLine +
                                  "<STRNGAYKY>{2}</STRNGAYKY>" + Environment.NewLine +
                                  "<STRNGUOIKY>{3}</STRNGUOIKY>" + Environment.NewLine +
                                  "<STRNOIGUI>{4}</STRNOIGUI>" + Environment.NewLine +
                                  "<STRLOAIVANBAN>{5}</STRLOAIVANBAN>" + Environment.NewLine +
                                  "<STRTDOFFICE>{6}</STRTDOFFICE>" + Environment.NewLine +
                                 "</EXPORTMAIL>";
            string inputValue = string.Format(fileFormat.Trim(), STRKYHIEU, STRTRICHYEU, STRNGAYKY, STRNGUOIKY, STRNOIGUI, STRLOAIVANBAN, "TDOFFICE");
            return inputValue;
        }
        public static string ConvertAnyFileToBase64String(string fileinputpath)
        {
            System.IO.FileStream inFile;
            byte[] binaryData;
            string base64String;
            try
            {
                inFile = new System.IO.FileStream(fileinputpath, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                binaryData = new Byte[inFile.Length];
                long bytesRead = inFile.Read(binaryData, 0, (int)inFile.Length);
                inFile.Close();
            }
            catch (Exception exp)
            {
                return "";
            }
            try
            {
                base64String = System.Convert.ToBase64String(binaryData, 0, binaryData.Length);
            }
            catch (ArgumentNullException)
            {
                return "";
            }
            return base64String;
            //File.WriteAllBytes("123.pdf", Convert.FromBase64String(base64String));
        }
        public static DataSet StringXmlToDataSet(string xmlvalue)
        {
            DataSet ds = new DataSet();
            StringReader reader = new StringReader(xmlvalue);
            ds.ReadXml(reader);
            return ds;
        }
        public static bool Connected(string URL)
        {
            try
            {
                IPHostEntry hostentry = Dns.GetHostEntry(URL);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
    public class WebClientWithCookies : WebClient
    {
        //private readonly CookieContainer _cookies = TDSMS.Classes.LotusConnections.cookies;
        //protected override WebRequest GetWebRequest(Uri uri)
        //{
        //    WebRequest request = base.GetWebRequest(uri);
        //    if (request is HttpWebRequest)
        //    {
        //        (request as HttpWebRequest).CookieContainer = _cookies;
        //    }
        //    return request;
        //}
    }
}

